package gz.aforo.model;

public interface Updateable {
	public void update(Object...objects);
}
