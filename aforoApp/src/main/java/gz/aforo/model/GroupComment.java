package gz.aforo.model;

import java.util.Date;

public class GroupComment {
    
   // private Integer id;
    private String user;
    private String groupName;
    private Date postDate;
    private String content;

    public GroupComment(String user, String group, Date postDate, String content) {
        //this.id = id;
        this.user = user;
        this.groupName = group;
        this.postDate = postDate;
        this.content = content;
    }

//    public Integer getId() {
//        return id;
//    }
    
    public String getUserName() {
        return user;
    }

    public String getGroupName() {
        return groupName;
    }
    
    public Date getPostDate() {
        return postDate;
    }

//    public void setId(Integer id) {
//        this.id = id;
//    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setGroupNem(String group) {
        this.groupName = group;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
//        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((postDate == null) ? 0 : postDate.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GroupComment other = (GroupComment) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (groupName == null) {
            if (other.groupName != null)
                return false;
        } else if (!groupName.equals(other.groupName))
            return false;
//        if (id == null) {
//            if (other.id != null)
//                return false;
//        } else if (!id.equals(other.id))
//            return false;
        if (postDate == null) {
            if (other.postDate != null)
                return false;
        } else if (!postDate.equals(other.postDate))
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        return true;
    }

    public String getContent() {
        return content;
    }

}
