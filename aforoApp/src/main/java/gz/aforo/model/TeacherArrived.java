package gz.aforo.model;

import java.util.Date;

public class TeacherArrived {
	private String subject;
	private String updateUser;
	private Date lastUpdate;
	private Boolean arrived;

	public String getUpdateUser() {
		return updateUser;
	}

	public TeacherArrived setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
		return this;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public TeacherArrived setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}

	public Boolean getArrived() {
		return arrived;
	}

	public TeacherArrived setArrived(Boolean arrived) {
		this.arrived = arrived;
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public boolean hasArrived(){
		return arrived == true;
	}
	
	public boolean notArrived(){
		return arrived == false;
	}
	
	@Override
	public String toString() {
		return "TeacherArrived [subject=" + subject + ", updateUser=" + updateUser + ", lastUpdate=" + lastUpdate
				+ ", arrived=" + arrived + "]";
	}

	public TeacherArrived(String subject, String updateUser) {
		super();
		this.subject = subject;
		this.updateUser = updateUser;
	}

	public TeacherArrived(String subject, String updateUser, Date lastUpdate, Boolean arrived) {
		super();
		this.subject = subject;
		this.updateUser = updateUser;
		this.lastUpdate = lastUpdate;
		this.arrived = arrived;
	}
}
