package gz.aforo.model;

import java.util.Date;

public class SubjectComment {
	private Integer id = -1;
	private String userName;
	private String subject;
	private Date postDate;
	private String content = "";
	private Integer likes = 0;
	private Integer dislikes = 0;
	private Boolean isBest = false;
	private Boolean urgent = false;

	public Boolean getIsBest() {
		return isBest;
	}

	public void setIsBest(Boolean isBest) {
		this.isBest = isBest;
	}

	public Boolean isUrgent() {
		return urgent;
	}

	public Boolean isNotUrgent() {
		return urgent == false;
	}

	public SubjectComment setUrgent(Boolean urgent) {
		this.urgent = urgent;
		return this;
	}

	public Boolean isBest() {
		return isBest;
	}

	public void setBest(Boolean isBest) {
		this.isBest = isBest;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public Integer getDislikes() {
		return dislikes;
	}

	public void setDislikes(Integer dislikes) {
		this.dislikes = dislikes;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SubjectComment(Integer id, String userName, String subject, Date postDate, String content, Integer likes, Integer dislikes) {
		super();
		this.id = id;
		this.userName = userName;
		this.subject = subject;
		this.postDate = postDate;
		this.content = content;
		this.likes = likes;
		this.dislikes = dislikes;
	}

	public SubjectComment(String userName, String subject, Date postDate, String content, Integer likes, Integer dislikes) {
		super();
		this.userName = userName;
		this.subject = subject;
		this.postDate = postDate;
		this.content = content;
		this.likes = likes;
		this.dislikes = dislikes;
	}

	public SubjectComment() {
		super();
	}

	public SubjectComment(String userName, String subject, String content) {
		super();
		this.userName = userName;
		this.subject = subject;
		this.content = content;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", userName=" + userName + ", subject=" + subject + ", postDate=" + postDate + ", content=" + content
				+ ", likes=" + likes + ", dislikes=" + dislikes + "]";
	}
}
