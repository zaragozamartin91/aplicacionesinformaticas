package gz.aforo.model;

import java.util.Date;

public class ReviewsComment {
    
    @Override
    public String toString() {
        return "ReviewsComment [User=" + User + ", Subject=" + Subject + ", content=" + content + ", date=" + date
                + "]";
    }

    private String User;
    private String Subject;
    private String content;
    private Date date;

    public ReviewsComment(String user2, String subject2, Date date, String content2) {
        User=user2;
        Subject=subject2;
        content=content2;
        this.date=date;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Subject == null) ? 0 : Subject.hashCode());
        result = prime * result + ((User == null) ? 0 : User.hashCode());
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReviewsComment other = (ReviewsComment) obj;
        if (Subject == null) {
            if (other.Subject != null)
                return false;
        } else if (!Subject.equals(other.Subject))
            return false;
        if (User == null) {
            if (other.User != null)
                return false;
        } else if (!User.equals(other.User))
            return false;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        return true;
    }
    public String getUser() {
        return User;
    }
    public void setUser(String user) {
        User = user;
    }
    public String getSubject() {
        return Subject;
    }
    public void setSubject(String subject) {
        Subject = subject;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    
    public void setPostDate(Date date) {
        this.date = date;
    }
    
    public Date getPostDate() {
        return date;
    }
    
}
