package gz.aforo.model;

public class SubjectInfo {
	private String name;
	private String info;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public SubjectInfo() {
		super();
	}

	public SubjectInfo(String name, String info) {
		super();
		this.name = name;
		this.info = info;
	}
}
