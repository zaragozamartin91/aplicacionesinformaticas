package gz.aforo.model;

import java.util.Arrays;
import java.util.List;

public class DataGroup {
	private List<Object> objects;

	public DataGroup(Object... objects) {
		this.objects = Arrays.asList(objects);
	}

	public Object get(Integer index) {
		return this.objects.get(index);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objects == null) ? 0 : objects.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataGroup other = (DataGroup) obj;
		if (objects == null) {
			if (other.objects != null)
				return false;
		} else if (!objects.equals(other.objects))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataGroup [objects=" + objects + "]";
	}
}
