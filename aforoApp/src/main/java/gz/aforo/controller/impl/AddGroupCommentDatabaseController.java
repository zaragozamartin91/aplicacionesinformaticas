package gz.aforo.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import gz.aforo.controller.AddGroupCommentController;
import gz.aforo.controller.AddSubjectCommentController;
import gz.aforo.controller.AddSubjectCommentController.ExtraOperation;
import gz.aforo.model.GroupComment;
import gz.aforo.model.SubjectComment;
import gz.aforo.service.GroupCommentService;
import gz.aforo.service.SubjectCommentService;

@Component("addGroupCommentDatabaseController")
@Scope("singleton")
public class AddGroupCommentDatabaseController implements AddGroupCommentController {
    @Autowired
    @Qualifier("groupCommentDatabaseService")
    private GroupCommentService groupCommentService;

    @Override
    public void addComment(GroupComment comment, ExtraOperation... extraOperations) {
        groupCommentService.addComment(comment);
        if (extraOperations != null && extraOperations.length > 0) {
            for (ExtraOperation extraOperation : extraOperations) {
                extraOperation.run();
            }
        }
    }
}
