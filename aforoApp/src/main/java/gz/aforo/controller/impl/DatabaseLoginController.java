package gz.aforo.controller.impl;

import gz.aforo.controller.LoginController;
import gz.aforo.model.LoginUser;
import gz.aforo.service.LoginUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class DatabaseLoginController implements LoginController {
	@Autowired
	@Qualifier("loginUserDatabaseService")
	private LoginUserService loginUserService;

	@Override
	public boolean checkLogin(String user, String password) {
		try {
			LoginUser loginUser = loginUserService.getUser(user);
			if (loginUser.getPass().equals(password)) {
				return true;
			}
		} catch (Exception e) {
			System.err.println("User " + user + " not found!");
			System.err.println("Error:" + e.getMessage());
		}

		return false;
	}
}
