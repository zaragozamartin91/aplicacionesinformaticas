package gz.aforo.controller.impl;

import gz.aforo.controller.SubjectCommentController;
import gz.aforo.model.SubjectComment;
import gz.aforo.service.SubjectCommentService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("subjectCommentDatabaseController")
@Scope("singleton")
public class SubjectCommentDatabaseController implements SubjectCommentController {
	@Autowired
	@Qualifier("subjectCommentDatabaseService")
	private SubjectCommentService commentService;

	private interface Operation {
		void exec(SubjectComment comment);
	}

	private Map<Action, Operation> operations = new HashMap<>();

	@PostConstruct
	private void initialize() {
		operations.put(Action.AUGMENT_LIKES, comm -> commentService.augmentLikes(comm));
		operations.put(Action.AUGMENT_DISLIKES, comm -> commentService.augmentDislikes(comm));
	}

	/* (non-Javadoc)
	 * @see gz.aforo.controller.impl.SubjectCommentController#run(gz.aforo.controller.impl.SubjectCommentDatabaseController.Action, gz.aforo.model.Comment)
	 */
	@Override
	public void run(Action action, SubjectComment comment) {
		operations.get(action).exec(comment);
	}
}
