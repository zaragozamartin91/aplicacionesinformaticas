package gz.aforo.controller.impl;

import static gz.aforo.controller.TeacherArrivedController.Action.TEACHER_ARRIVED;
import static gz.aforo.controller.TeacherArrivedController.Action.TEACHER_LEFT;
import gz.aforo.controller.TeacherArrivedController;
import gz.aforo.model.TeacherArrived;
import gz.aforo.model.Updateable;
import gz.aforo.service.TeacherArrivedService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class TeacherArrivedDatabaseController implements TeacherArrivedController {
	@Autowired
	@Qualifier("teacherArrivedDatabaseService")
	private TeacherArrivedService teacherArrivedService;

	private Map<Object, Updateable> updateables = new HashMap<>();

	@Override
	public void addUpdateable(Object upId, Updateable updateable) {
		Optional.ofNullable(updateables).filter(upds -> upds.containsKey(updateable) == false)
				.ifPresent(upds -> upds.put(upId, updateable));
	}

	@Override
	public void run(Action action, String subject, String userName) {
		TeacherArrived teacherArrived = new TeacherArrived(subject, userName);
		TeacherArrived lastTeacherArrivedReg = Optional.ofNullable(teacherArrivedService.getLast(subject)).orElse(
				new TeacherArrived("", "", new Date(), false));

		Optional.of(action).filter(act -> act.equals(TEACHER_ARRIVED) && lastTeacherArrivedReg.notArrived())
				.ifPresent(act -> teacherArrivedService.setArrived(teacherArrived.setArrived(true)));

		Optional.of(action).filter(act -> act.equals(TEACHER_LEFT) && lastTeacherArrivedReg.hasArrived())
				.ifPresent(act -> teacherArrivedService.setArrived(teacherArrived.setArrived(false)));

		TeacherArrived newTeacherArrivedReg = teacherArrivedService.getLast(subject);
		if (newTeacherArrivedReg == null) {
			return;
		}
		updateables.keySet().stream().filter(key -> key.equals(subject)).forEach(key -> updateables.get(key).update(newTeacherArrivedReg));
	}
}
