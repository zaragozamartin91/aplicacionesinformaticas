package gz.aforo.controller.impl;

import gz.aforo.controller.LoginController;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class DummyLoginController implements LoginController {
	private Map<String, String> usersMap = new HashMap<>();
	
	public DummyLoginController(){
		usersMap.put("martin", "martin");
		usersMap.put("nadia", "nadia");
	}
	
	@Override
	public boolean checkLogin(String user,String password){
		return usersMap.containsKey(user) && usersMap.get(user).equals(password);
	}
}
