package gz.aforo.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import gz.aforo.controller.AddSubjectCommentController;
import gz.aforo.model.SubjectComment;
import gz.aforo.service.SubjectCommentService;

@Component("addSubjectCommentDatabaseController")
@Scope("singleton")
public class AddSubjectCommentDatabaseController implements AddSubjectCommentController {
    @Autowired
    @Qualifier("subjectCommentDatabaseService")
    private SubjectCommentService subjectCommentService;

    @Override
    public void addComment(SubjectComment comment, ExtraOperation... extraOperations) {
        subjectCommentService.addComment(comment);
        if (extraOperations != null && extraOperations.length > 0) {
            for (ExtraOperation extraOperation : extraOperations) {
                extraOperation.run();
            }
        }
    }
}
