package gz.aforo.controller;

public interface LoginController {

	/**
	 * Verifica login.
	 * 
	 * @param user
	 *            - Usuario.
	 * @param password
	 *            - Password.
	 * @return True en caso de login correcto, false en caso contrario.
	 */
	public abstract boolean checkLogin(String user, String password);

}