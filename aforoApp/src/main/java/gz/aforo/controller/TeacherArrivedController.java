package gz.aforo.controller;

import gz.aforo.model.Updateable;

public interface TeacherArrivedController {
	public enum Action {
		TEACHER_ARRIVED, TEACHER_LEFT
	};

	/**
	 * Agrega un registro de arribo o salida del profesor.
	 * 
	 * @param action
	 *            - Accion de arribo o salida.
	 * @param subject
	 *            - Catedra o materia.
	 * @param userName
	 *            - Nombre de usuario.
	 */
	void run(Action action, String subject, String userName);

	default void run(Action action, String userName) {
		run(action, "DEFAULT", userName);
	}

	public void addUpdateable(Object upId, Updateable updateable);
}