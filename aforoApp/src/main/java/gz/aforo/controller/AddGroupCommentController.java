package gz.aforo.controller;

import gz.aforo.controller.AddSubjectCommentController.ExtraOperation;
import gz.aforo.model.GroupComment;
import gz.aforo.model.SubjectComment;

public interface AddGroupCommentController {

    public void addComment(GroupComment comment, ExtraOperation... extraOperations);
    
}
