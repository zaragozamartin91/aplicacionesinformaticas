package gz.aforo.controller;

import gz.aforo.model.SubjectComment;

public interface SubjectCommentController {
	public enum Action {
		AUGMENT_LIKES, AUGMENT_DISLIKES
	};

	public abstract void run(Action action, SubjectComment comment);

}