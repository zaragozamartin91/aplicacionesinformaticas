package gz.aforo.controller;

import gz.aforo.model.SubjectComment;

public interface AddSubjectCommentController {
    public void addComment(SubjectComment comment, ExtraOperation... extraOperations);

    @FunctionalInterface
    public static interface ExtraOperation {
        void run();
    }
}
