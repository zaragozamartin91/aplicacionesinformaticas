package gz.aforo.util;

@FunctionalInterface
public interface SimpleOperation {
    void run();
}
