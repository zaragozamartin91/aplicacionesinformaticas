package gz.aforo.config;


public class RunMode {
	private String mode;

	public RunMode(String mode) {
		super();
		this.mode = mode;
	}

	public boolean is(String mode) {
		return this.mode.equals(mode);
	}
}
