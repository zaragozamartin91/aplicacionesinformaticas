package gz.aforo.config.spring;

import gz.aforo.config.RunMode;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
@ComponentScan(basePackages = { "gz.aforo.controller", "gz.aforo.view", "gz.aforo.service" })
public class SpringMainConfig {
	@Bean(name = "configurationProperties")
	@Scope("singleton")
	public static Properties configurationProperties() {
		try {
			Properties properties = new Properties();
			properties.load(new ClassPathResource("configuration.properties").getInputStream());
			return properties;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer onePlaceholder(
			@Qualifier("configurationProperties") Properties properties) {
		PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		propertySourcesPlaceholderConfigurer.setProperties(properties);
		return propertySourcesPlaceholderConfigurer;
	}

	@Bean(name = "dbConnection")
	@Scope("prototype")
	public Connection getConnection(@Qualifier("configurationProperties") Properties properties) {
		try {
			String jdbcDriver = properties.getProperty("JDBC_DRIVER");
			Class.forName(jdbcDriver);
			String url = properties.getProperty("DB_URL");
			String user = properties.getProperty("DB_USER");
			String password = properties.getProperty("DB_PASS");
			return DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Bean(name = "runMode")
	@Scope("singleton")
	public RunMode runMode(@Value("${runMode}") String mode) {
		return new RunMode(mode);
	}
}
