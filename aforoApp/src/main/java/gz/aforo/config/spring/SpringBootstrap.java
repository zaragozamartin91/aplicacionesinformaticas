package gz.aforo.config.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Clase que inicia el configurador de spring y a la cual se le pueden solicitar
 * beans.
 * 
 * @author martin
 *
 */
public class SpringBootstrap {
	private static SpringBootstrap instance = new SpringBootstrap();
	private ApplicationContext applicationContext;

	private SpringBootstrap() {
		applicationContext = new AnnotationConfigApplicationContext(SpringMainConfig.class);
	}

	public static SpringBootstrap get() {
		return instance;
	}

	public ApplicationContext context() {
		return applicationContext;
	}

	/**
	 * Obtiene un bean de spring.
	 * 
	 * @param name
	 *            - Nombre de bean.
	 * @return Bean recuperado
	 */
	public Object bean(String name) {
		return applicationContext.getBean(name);
	}
}
