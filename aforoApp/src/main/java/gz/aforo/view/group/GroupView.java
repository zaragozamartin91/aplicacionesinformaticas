package gz.aforo.view.group;

import gz.aforo.controller.AddGroupCommentController;
import gz.aforo.model.GroupComment;
import gz.aforo.service.GroupCommentService;
import gz.aforo.service.GroupService;
import gz.aforo.view.SessionKeys;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("groupView")
@Scope("prototype")
public class GroupView extends VerticalLayout implements View {
	public static final String STYLE_NAME = "main";
	public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";
	private static final int COMMENTS_LIMIT = 5;

	private HorizontalLayout header = new HorizontalLayout();
	private MenuBar menuBar;
	private VerticalLayout commentsLayout = new VerticalLayout();
	private VerticalLayout mainLayout = new VerticalLayout();
	private HorizontalLayout openFilesLayout = null;
	private Button openFilesButton;
	private Button leaveGroupButton;
	private TextArea newCommentTextArea = null;
	private VerticalLayout addCommentLayout;
	private Label title;
	private Tree usersTree;

	@Autowired
	@Qualifier("groupCommentDatabaseService")
	private GroupCommentService commentService;

	@Autowired
	@Qualifier("addGroupCommentDatabaseController")
	private AddGroupCommentController addGroupCommentController;

	@Autowired
	@Qualifier("groupDatabaseService")
	private GroupService groupService;

	@Autowired
	private ApplicationContext applicationContext;

	public GroupView() {
		this.setStyleName(STYLE_NAME);
		this.setWidth("100%");
		this.setHeightUndefined();
	}

	@PostConstruct
	private void initialize() {
		setHeader();
		setTitle();
		setMenuBar();

		this.mainLayout.setSpacing(true);
		this.mainLayout.setMargin(true);
		this.addComponent(mainLayout);
	}

	private void setMenuBar() {
		menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
		this.addComponent(menuBar);
	}

	private void setHeader() {
		header.setSizeFull();
		header.setSpacing(true);
		header.setMargin(true);
		this.addComponent(header);
	}

	private String getCurrUser() {
		String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
		return userName;
	}

	private void setTitle() {
		title = new Label("Aforo");
		title.setSizeUndefined();
		title.setStyleName("h1");
		header.addComponent(title);
		header.setComponentAlignment(title, Alignment.TOP_RIGHT);
		header.setExpandRatio(title, 1);
	}

	private String getCurrentSubject() {
		if (getSession() == null) {
			return DEFAULT_SUBJECT;
		}
		return Optional.ofNullable(getSession().getAttribute(SessionKeys.SUBJECT).toString()).orElse(DEFAULT_SUBJECT);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		System.out.println("entering GroupView....");

		title.setValue(getCurrUser() + "@" + getCurrentSubject());
		if (!isMember()) {
			// Se le indica que debe inscribirse primero
			showNotMemberView();
			return;
		}

		if (openFilesLayout == null) {
			setOpenFilesAndLeaveGroupButtons();
		}

		if (usersTree == null) {
			setUsersTree();
		}

		if (newCommentTextArea == null) {
			setAddComment();
		}

		setRecentComments();
	}

	private boolean isMember() {
		String currentSubject = getCurrentSubject();
		String currUser = getCurrUser();
		List<String> signedUsers = groupService.getUserOf(currentSubject);
		return signedUsers.contains(currUser);
	}

	private void showNotMemberView() {
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setMargin(true);
		addComponent(buttonsLayout);
		Button button = new Button("Enviar solicitud de aceptación al grupo", FontAwesome.ASTERISK);
		button.setSizeUndefined();
		button.setHeight("100%");
		button.addClickListener(evt -> {
			String userName = getSession().getAttribute("user").toString();
			Notification.show("El usuario " + userName + " intenta unirse al grupo. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
		});
		buttonsLayout.addComponent(button);
		this.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_CENTER);
	}

	private void setUsersTree() {
		usersTree = new Tree();
		String group = getCurrentSubject();
		usersTree = new Tree("Integrantes");
		usersTree.addItem(group);
		List<String> usersGroup = groupService.getUserOf(group);
		System.out.println("Lista de usuarios: " + usersGroup.toString() + " del grupo: " + group);
		for (int j = 0; j < usersGroup.size(); j++) {
			String user = usersGroup.get(j);
			usersTree.addItem(user);
			usersTree.setParent(user, group);
			usersTree.setChildrenAllowed(user, false);
		}
		usersTree.expandItemsRecursively(group);

		VerticalLayout usersTreeLayout = new VerticalLayout(usersTree);
		usersTreeLayout.setMargin( new MarginInfo(false, true, false, true) );
		usersTreeLayout.setWidth("100%");
		usersTreeLayout.setSpacing(true);
		
		mainLayout.addComponent(usersTreeLayout);

		usersTree.setImmediate(true);
		// POR EL MOMENTO NO QUEREMOS QUE SE PUEDA HACER NADA CON ESTA TABLA.
		// users.addValueChangeListener(e -> {
		// String userName = getSession().getAttribute("user").toString();
		// String nombreDeCatedra = String.valueOf(e.getProperty().getValue());
		// System.out.println("parent..."+users.getParent(nombreDeCatedra));
		// if ((nombreDeCatedra.equals("null") ||
		// (users.getParent(nombreDeCatedra) == null)))
		// return;
		// System.out.println(userName+" selecciono:  " + nombreDeCatedra);
		// getSession().setAttribute("subject", nombreDeCatedra);
		// getUI().getNavigator().addView("main", (View)
		// SpringBootstrap.get().bean("mainView"));
		// getUI().getNavigator().navigateTo("main");
		// });
	}

	private void setOpenFilesAndLeaveGroupButtons() {
		openFilesLayout = new HorizontalLayout();
		openFilesLayout.setMargin(new MarginInfo(false, true, false, true));
		openFilesLayout.setSpacing(true);
		openFilesLayout.setSizeUndefined();
		openFilesLayout.setWidth("100%");

		openFilesButton = new Button("Buscar archivos", FontAwesome.FILE);
		openFilesButton.setSizeFull();
		openFilesLayout.addComponent(openFilesButton);

		openFilesButton.addClickListener(evt -> {
			System.out.println("Abrir archivos...");
			getUI().getNavigator().navigateTo("file");
		});

		openFilesLayout.setMargin(true);

		leaveGroupButton = new Button("Salir del grupo", FontAwesome.TRASH_O);
		leaveGroupButton.setSizeFull();
		leaveGroupButton.addClickListener(evt -> {
			String userName = getSession().getAttribute("user").toString();
			Notification.show("El usuario " + userName + " intenta salirse del grupo. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
		});

		openFilesLayout.addComponent(leaveGroupButton);

		mainLayout.addComponent(openFilesLayout);
	}

	private void setAddComment() {
		addCommentLayout = new VerticalLayout();
		addCommentLayout.setSizeUndefined();
		addCommentLayout.setWidth("100%");
		addCommentLayout.setSpacing(true);
		addCommentLayout.setMargin(true);

		newCommentTextArea = new TextArea("Nuevo comentario");
		newCommentTextArea.setSizeFull();
		addCommentLayout.addComponent(newCommentTextArea);

		Button submitButton = new Button("Agregar nuevo comentario");
		submitButton.setSizeFull();
		HorizontalLayout urgentAndSubmitLayout = new HorizontalLayout(submitButton);
		urgentAndSubmitLayout.setSizeFull();
		urgentAndSubmitLayout.setExpandRatio(submitButton, 1);
		urgentAndSubmitLayout.setSpacing(true);

		submitButton.addClickListener(evt -> {
			String content = newCommentTextArea.getValue();
			String userName = getCurrUser();
			String group = getCurrentSubject();
			GroupComment newComment = new GroupComment(userName, group, new Date(), content);
			this.addGroupCommentController.addComment(newComment);
			this.setRecentComments();
			// Reseteo el comentario
				newCommentTextArea.setValue("");
				// isUrgentCheckBox.setValue(false);
			});

		addCommentLayout.addComponent(urgentAndSubmitLayout);
		mainLayout.addComponent(addCommentLayout);
		// this.addComponent(addCommentLayout);
	}

	private void setRecentComments() {
		try {
			mainLayout.removeComponent(commentsLayout);
		} catch (Exception e) {
		}

		/* Se resetea la seccion de comentarios */
		commentsLayout = new VerticalLayout();

		this.commentsLayout.setSpacing(true);
		this.commentsLayout.setMargin(true);
		mainLayout.addComponent(commentsLayout);

		String groupName = getCurrentSubject();

		List<GroupComment> comments = commentService.getCommentsOf(groupName, COMMENTS_LIMIT);

		VerticalLayout bestCommentsLayout = new VerticalLayout();
		bestCommentsLayout.setSpacing(true);
		comments.forEach(comm -> {
			GroupCommentLayout commentLayout = new GroupCommentLayout(comm);
			bestCommentsLayout.addComponent(commentLayout);
		});
		commentsLayout.addComponent(bestCommentsLayout);
	}

}
