package gz.aforo.view.group;

import gz.aforo.controller.SubjectCommentController;
import gz.aforo.controller.SubjectCommentController.Action;
import gz.aforo.model.GroupComment;
import gz.aforo.model.SubjectComment;
import gz.aforo.util.SimpleOperation;

import java.util.Date;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class GroupCommentLayout extends HorizontalLayout {
    public static final String STYLE_NAME = "comment";
    public static final String BEST_STYLE_NAME = "bestComment";

    private String userName;
    private String content;
    private VerticalLayout userAndContentLayout = new VerticalLayout();
    private Date postDate;

    private GroupComment comment;

    public GroupCommentLayout(GroupComment c) {
        this.comment = c;
        init(c.getUserName(), c.getGroupName(), c.getPostDate(), c.getContent());
    }

    private void init(String userName, String group, Date postDate, String content) {
        this.userName = userName.startsWith("@") ? userName : "@".concat(userName);
        this.content = content;
        this.postDate = postDate;

        this.setSizeFull();
        this.setSpacing(true);
        this.setMargin(true);

        this.setStyleName(STYLE_NAME);

        this.addComponent(userAndContentLayout);

        this.setExpandRatio(userAndContentLayout, 10);

        buildUserAndContent();
//        buildLikes();
    }

//    private void buildLikes() {
//        // likesLayout.setSizeFull();
//        likesLayout.setSizeUndefined();
//        VerticalLayout vl = new VerticalLayout();
//        likesLayout.addComponent(vl);
//
//        likeButton = new Button(FontAwesome.THUMBS_UP);
//        likesCountLabel = new Label(likes.toString());
//        dislikeButton = new Button(FontAwesome.THUMBS_DOWN);
//        dislikesCountLabel = new Label(dislikes.toString());
//
//        likeButton.setSizeFull();
//        likesCountLabel.setSizeFull();
//        dislikeButton.setSizeFull();
//        dislikesCountLabel.setSizeFull();
//
//        HorizontalLayout likeHorizontalLayout = new HorizontalLayout(likeButton, likesCountLabel);
//        HorizontalLayout dislikeHorizontalLayout = new HorizontalLayout(dislikeButton, dislikesCountLabel);
//
//        likeHorizontalLayout.setSizeFull();
//        likeHorizontalLayout.setSpacing(true);
//        dislikeHorizontalLayout.setSizeFull();
//        dislikeHorizontalLayout.setSpacing(true);
//
//        vl.addComponent(likeHorizontalLayout);
//        vl.addComponent(dislikeHorizontalLayout);
//        vl.setSizeFull();
//        vl.setSpacing(true);
//        vl.setMargin(true);
//
//        this.setComponentAlignment(likesLayout, Alignment.MIDDLE_RIGHT);
//        Responsive.makeResponsive(likeHorizontalLayout, dislikeHorizontalLayout, vl, likesLayout);
//    }

    private void buildUserAndContent() {
        userAndContentLayout.setSizeFull();
        VerticalLayout vl = new VerticalLayout();
        userAndContentLayout.addComponent(vl);
        vl.setSizeFull();
        String userNameContent = userName;
        Label userNameLabel = new Label(userNameContent);
//        if (comment.isBest()) {
//            userNameLabel.setContentMode(ContentMode.HTML);
//        }
        userNameLabel.setStyleName("h2");

        Label postDateLabel = new Label(postDate.toString());
        postDateLabel.setSizeUndefined();

        // userNameLabel.setIcon(FontAwesome.CHECK);

        HorizontalLayout userAndDateHorizontalLayout = new HorizontalLayout(userNameLabel, postDateLabel);
        userAndDateHorizontalLayout.setSpacing(true);
        userAndDateHorizontalLayout.setWidth("100%");
        userAndDateHorizontalLayout.setComponentAlignment(postDateLabel, Alignment.MIDDLE_RIGHT);

        vl.addComponent(userAndDateHorizontalLayout);
        Label contentLabel = new Label(content);
        vl.addComponent(contentLabel);
    }

//    public GroupCommentLayout setController(SubjectCommentController controller,
//            SimpleOperation... extraContollerOperations) {
//        Action al = SubjectCommentController.Action.AUGMENT_LIKES;
//        Action ad = SubjectCommentController.Action.AUGMENT_DISLIKES;
//
//        likeButton.addClickListener(evt -> {
//            controller.run(al, comment);
//            if (extraContollerOperations != null && extraContollerOperations.length > 0) {
//                for (SimpleOperation simpleOperation : extraContollerOperations) {
//                    simpleOperation.run();
//                }
//            }
//            // likesCountLabel.setValue((++likes).toString());
//            });
//        dislikeButton.addClickListener(evt -> {
//            controller.run(ad, comment);
//            if (extraContollerOperations != null && extraContollerOperations.length > 0) {
//                for (SimpleOperation simpleOperation : extraContollerOperations) {
//                    simpleOperation.run();
//                }
//            }
//            // dislikesCountLabel.setValue((++dislikes).toString());
//            });
//
//        return this;
//    }
}
