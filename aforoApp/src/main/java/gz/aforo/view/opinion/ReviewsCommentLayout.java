package gz.aforo.view.opinion;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import gz.aforo.model.GroupComment;
import gz.aforo.model.ReviewsComment;

import java.util.Date;

@SuppressWarnings("serial")
public class ReviewsCommentLayout extends HorizontalLayout {
    public static final String STYLE_NAME = "comment";
    public static final String BEST_STYLE_NAME = "bestComment";

    private String userName;
    private String content;
    private VerticalLayout userAndContentLayout = new VerticalLayout();
    private Date postDate;

    @SuppressWarnings("unused")
    private ReviewsComment comment;

    public ReviewsCommentLayout(ReviewsComment c) {
        this.comment = c;
        init(c.getUser(), c.getSubject(), c.getPostDate(), c.getContent());
    }

    private void init(String userName, String group, Date postDate, String content) {
        this.userName = userName.startsWith("@") ? userName : "@".concat(userName);
        this.content = content;
        this.postDate = postDate;

        this.setSizeFull();
        this.setSpacing(true);
        this.setMargin(true);

        this.setStyleName(STYLE_NAME);

        this.addComponent(userAndContentLayout);

        this.setExpandRatio(userAndContentLayout, 10);

        buildUserAndContent();
//        buildLikes();
    }


    private void buildUserAndContent() {
        userAndContentLayout.setSizeFull();
        VerticalLayout vl = new VerticalLayout();
        userAndContentLayout.addComponent(vl);
        vl.setSizeFull();
//        String userNameContent = userName;
//        Label userNameLabel = new Label(userNameContent);
//        if (comment.isBest()) {
//            userNameLabel.setContentMode(ContentMode.HTML);
//        }
//        userNameLabel.setStyleName("h2");

        Label postDateLabel = new Label(postDate.toString());
        postDateLabel.setSizeUndefined();

        // userNameLabel.setIcon(FontAwesome.CHECK);

        HorizontalLayout userAndDateHorizontalLayout = new HorizontalLayout(postDateLabel);//userNameLabel, postDateLabel);
        userAndDateHorizontalLayout.setSpacing(true);
        userAndDateHorizontalLayout.setWidth("100%");
        userAndDateHorizontalLayout.setComponentAlignment(postDateLabel, Alignment.MIDDLE_RIGHT);

        vl.addComponent(userAndDateHorizontalLayout);
        Label contentLabel = new Label(content);
        contentLabel.setContentMode(ContentMode.HTML);
        vl.addComponent(contentLabel);
    }

}
