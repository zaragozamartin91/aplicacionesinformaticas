package gz.aforo.view.opinion;

import gz.aforo.model.ReviewsComment;
import gz.aforo.service.GroupService;
import gz.aforo.service.ReviewsCommentService;
import gz.aforo.view.SessionKeys;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("opinionesView")
@Scope("prototype")
public class OpinionsView extends VerticalLayout implements View {
    public static final String STYLE_NAME = "main";
    public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";
    private static final int COMMENTS_LIMIT = 5;

    private HorizontalLayout header = new HorizontalLayout();
    private MenuBar menuBar;
    private VerticalLayout commentsLayout = new VerticalLayout();
    private VerticalLayout commentsAndUserLayout = new VerticalLayout();
    
    @Autowired
    @Qualifier("reviewsCommentDatabaseService")
    private ReviewsCommentService commentService;
    
    @Autowired
    @Qualifier("groupDatabaseService")
    private GroupService groupService;

    private Label title;
    
    @Autowired
    private ApplicationContext applicationContext;

    public OpinionsView() {
        this.setStyleName(STYLE_NAME);
        this.setWidth("100%");
        this.setHeightUndefined();
    }

    @PostConstruct
    private void initialize() {
        setHeader();
        setTitle();
        setMenuBar();
    }

    private void setMenuBar() {
        menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
        this.addComponent(menuBar);
    }

    private void setHeader() {
        header.setSizeFull();
        header.setSpacing(true);
        header.setMargin(true);
        this.addComponent(header);
    }

    private String getCurrUser() {
        String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
        return userName;
    }

    private void setTitle() {
        title = new Label("Aforo");
        title.setSizeUndefined();
        title.setStyleName("h1");
        header.addComponent(title);
        header.setComponentAlignment(title, Alignment.TOP_RIGHT);
        header.setExpandRatio(title, 1);
    }

    private String getCurrentSubject() {
        if (getSession() == null) {
            return DEFAULT_SUBJECT;
        }
        return Optional.ofNullable(getSession().getAttribute(SessionKeys.SUBJECT).toString()).orElse(DEFAULT_SUBJECT);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        System.out.println("entering OpinionesView....");

        Object upId = getCurrentSubject();

        title.setValue(getCurrUser() + "::Opiniones" + getCurrentSubject());
        
        this.commentsAndUserLayout.setSpacing(true);
        this.commentsAndUserLayout.setMargin(true);
        this.addComponent(commentsAndUserLayout);
        
        setRecentComments();
    }
    
    private void setRecentComments() {
        try {
            this.removeComponent(commentsLayout);
        } catch (Exception e) {
        }

        /* Se resetea la seccion de comentarios */
        commentsLayout = new VerticalLayout();

        this.commentsLayout.setSpacing(true);
        this.commentsLayout.setMargin(true);
        //this.addComponent(commentsLayout);
        commentsAndUserLayout.addComponent(commentsLayout);

        String subject = getCurrentSubject();

        List<ReviewsComment> comments = commentService.getCommentsOf(subject, COMMENTS_LIMIT);

        VerticalLayout bestCommentsLayout = new VerticalLayout();
        bestCommentsLayout.setSpacing(true);
        comments.forEach(comm -> {
            ReviewsCommentLayout commentLayout = new ReviewsCommentLayout(comm);
            bestCommentsLayout.addComponent(commentLayout);
        });
        commentsLayout.addComponent(bestCommentsLayout);
    }
}
