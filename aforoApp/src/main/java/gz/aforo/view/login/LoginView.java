package gz.aforo.view.login;

import gz.aforo.config.RunMode;
import gz.aforo.controller.LoginController;
import gz.aforo.view.SessionKeys;
import gz.aforo.view.subject.home.SubjectHomeView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("loginView")
@Scope("prototype")
public class LoginView extends VerticalLayout implements View {
	@Autowired
	@Qualifier("databaseLoginController")
	private LoginController loginController;

	@Autowired
	@Qualifier("runMode")
	private RunMode runMode;

	private VerticalLayout content = new VerticalLayout();
	private TextField userField = new TextField("Usuario");
	private PasswordField passwordField = new PasswordField("Contraseña");
	private Button loginButton = new Button("Log In", FontAwesome.SIGN_IN);

	public LoginView() {
		super();

		initView();

		loginButton.addClickListener(evt -> {
			String user = userField.getValue();
			String password = passwordField.getValue();

			if (loginController.checkLogin(user, password)) {
				Notification.show("Login ok!");
				getSession().setAttribute(SessionKeys.USERNAME, user);

				// TODO: ESTABLEZCO UNA MATERIA/CATEDRA POR DEFECTO A MODO
				// DEBUG... EL ATRIBUTO DE SESION "subject" DEBE SETEARSE A
				// PARTIR DE LA SELECCION DE LA CATEDRA
				getSession().setAttribute(SessionKeys.SUBJECT, SubjectHomeView.DEFAULT_SUBJECT);

				getUI().getNavigator().navigateTo("home");
			} else {
				Notification.show("Inicio de sesion fallo!", Type.ERROR_MESSAGE);
				passwordField.setValue("");
				passwordField.focus();
			}
		});
	}

	private void initView() {
		this.addStyleName("login");
		this.setSizeFull();
		content.setSizeUndefined();
		content.setSpacing(true);
		this.addComponent(content);
		this.setComponentAlignment(content, Alignment.MIDDLE_CENTER);

		userField.setIcon(FontAwesome.USER);
		passwordField.setIcon(FontAwesome.KEY);
		loginButton.setWidth("100%");

		content.addComponent(new Label("Iniciar sesión"));
		content.addComponent(userField);
		content.addComponent(passwordField);
		content.addComponent(loginButton);

		loginButton.setClickShortcut(KeyCode.ENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		System.out.println("entering LoginView...");
		clearForm();
		userField.focus();
	}

	private void clearForm() {
		this.userField.clear();
		this.passwordField.clear();
	}
}
