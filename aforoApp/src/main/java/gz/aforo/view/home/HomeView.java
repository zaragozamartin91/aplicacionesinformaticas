package gz.aforo.view.home;

import gz.aforo.config.spring.SpringBootstrap;
import gz.aforo.controller.TeacherArrivedController;
import gz.aforo.service.GroupService;
import gz.aforo.service.TeacherArrivedService;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("homeView")
@Scope("prototype")
public class HomeView extends VerticalLayout implements View {
	public static final String STYLE_NAME = "main";
	public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";

	private HorizontalLayout header = new HorizontalLayout();
	private MenuBar menuBar;
	private HorizontalLayout commentsLayout = new HorizontalLayout();
	private Tree tree;
	private Tree users = null;

	@Autowired
	@Qualifier("groupDatabaseService")
	private GroupService groupService;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	@Qualifier("teacherArrivedDatabaseController")
	private TeacherArrivedController teacherArrivedController;

	@Autowired
	@Qualifier("teacherArrivedDatabaseService")
	private TeacherArrivedService teacherArrivedService;

	public HomeView() {
		this.setStyleName(STYLE_NAME);

		this.setWidth("100%");
		this.setHeightUndefined();
	}

	@PostConstruct
	private void initialize() {
		setHeader();
		setTitle();
		setMenuBar();

		// se agregan comentarios de prueba
		setSampleComments();
		// Creo el menu
		setSubjectsTable();
		setGroupsTable();
	}

	private void setSampleComments() {
		this.commentsLayout.setSpacing(true);
		this.commentsLayout.setMargin(true);
		this.addComponent(commentsLayout);
	}

	private void setMenuBar() {
		menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
		this.addComponent(menuBar);
	}

	private void setHeader() {
		header.setSizeFull();
		header.setSpacing(true);
		header.setMargin(true);
		this.addComponent(header);
	}

	private void setTitle() {
		Label title = new Label("Aforo");
		title.setSizeUndefined();
		title.setStyleName("h1");
		header.addComponent(title);
		header.setComponentAlignment(title, Alignment.TOP_RIGHT);
		header.setExpandRatio(title, 1);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		System.out.println("entering Home view....");
	}

	private void setSubjectsTable() {
		/*
		 * You can read or set the currently selected item by the value property
		 * of the Tree component, that is, with getValue() and setValue(). When
		 * the user clicks an item on a tree, the tree will receive an
		 * ValueChangeEvent, which you can catch with a ValueChangeListener. To
		 * receive the event immediately after the click, you need to set the
		 * tree as setImmediate(true).
		 * 
		 * The Tree component uses Container data sources much like the Table
		 * component, with the addition that it also utilizes hierarchy
		 * information maintained by a HierarchicalContainer. The contained
		 * items can be of any item type supported by the container. The default
		 * container and its addItem() assume that the items are strings and the
		 * string value is used as the item ID.
		 */
		final Object[][] departamentos = new Object[][] { new Object[] { "Matematicas", "Probabilidad y estadisticas B" },
				new Object[] { "Fisica", "Fisica I", "Fisica II" },
				new Object[] { "Computacion", "Algoritmos y programacion I", "Taller de programacion I", "Aplicaciones informaticas" },
				new Object[] { "Electronica", "Laboratorio", "Organizacion de las computadoras" } };

		tree = new Tree("Departamentos");
		for (int i = 0; i < departamentos.length; i++) {
			String depto = (String) (departamentos[i][0]);
			tree.addItem(depto);
			if (departamentos[i].length == 1) {
				tree.setChildrenAllowed(depto, false);
			} else {
				for (int j = 1; j < departamentos[i].length; j++) {
					String catedra = (String) departamentos[i][j];
					tree.addItem(catedra);
					tree.setParent(catedra, depto);
					tree.setChildrenAllowed(catedra, false);
				}
				tree.expandItemsRecursively(depto);
			}
		}
		commentsLayout.addComponent(tree);
		tree.setImmediate(true);
		tree.addValueChangeListener(e -> {
			String userName = getSession().getAttribute("user").toString();
			String nombreDeCatedra = String.valueOf(e.getProperty().getValue());
			System.out.println("parent..." + tree.getParent(nombreDeCatedra));
			if ((nombreDeCatedra.equals("null") || (tree.getParent(nombreDeCatedra) == null)))
				return;
			System.out.println(userName + " selecciono:  " + nombreDeCatedra);
			tree.unselect(nombreDeCatedra);
			getSession().setAttribute("subject", nombreDeCatedra);
			getUI().getNavigator().navigateTo("main");
		});
	}

	private void setGroupsTable() {
		users = new Tree();
		List<String> groups = groupService.getGroupsNames();
		System.out.println("Lista de grupos: " + groups.toString());
		users = new Tree("Grupos");
		for (int i = 0; i < groups.size(); i++) {
			String group = groups.get(i);
			users.addItem(group);
			users.setChildrenAllowed(group, false);
		}
		commentsLayout.addComponent(users);
		users.setImmediate(true);
		users.addValueChangeListener(e -> {
			String userName = getSession().getAttribute("user").toString();
			String nombreDelGrupo = String.valueOf(e.getProperty().getValue());
			if (nombreDelGrupo.equals("null"))
				return;
			System.out.println(userName + " selecciono:  " + nombreDelGrupo);
			users.unselect(nombreDelGrupo);
			getSession().setAttribute("subject", nombreDelGrupo);
			getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
			getUI().getNavigator().navigateTo("group");
		});
	}

}
