package gz.aforo.view.pruebas;

import javax.annotation.PostConstruct;

import gz.aforo.controller.TeacherArrivedController;
import gz.aforo.model.TeacherArrived;
import gz.aforo.service.TeacherArrivedService;
import gz.aforo.view.subject.home.UpdateableTeacherArrivedLabel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PruebaDeView extends VerticalLayout implements View {
    public static final String STYLE_NAME = "prueba";

    private HorizontalLayout header = new HorizontalLayout();
    private Button teacherArrivedButton;
    private Button teacherLeftButton;
    // private Button approvalButton;
    // private Button disapprovalButton;
    private MenuBar menuBar;
    private Label teacherArrivedInfo;
    private VerticalLayout comments = new VerticalLayout();

    @Autowired
    @Qualifier("teacherArrivedDatabaseController")
    private TeacherArrivedController teacherArrivedController;

    @Autowired
    @Qualifier("teacherArrivedDatabaseService")
    private TeacherArrivedService teacherArrivedService;

    public PruebaDeView() {
        this.setStyleName("prueba");

        this.setWidth("100%");
        this.setHeightUndefined();
    }

    @PostConstruct
    private void initialize() {
        setHeader();
        setTeacherArrivedButton();
        setTeacherLeftButton();
        setTeacherArrivedInfo();
        setTitle();
        setMenuBar();
        setCommets();
        // Es para agregar nuevos comentarios
        setNewComment();
        // Imprime los comentarios que ya existian
        setOldComment();
        // setApprovalButton();
        // setDisapprovalButton();
    }

    private void setTeacherArrivedInfo() {
        teacherArrivedInfo = new Label();
        teacherArrivedInfo.setSizeUndefined();
        header.addComponent(teacherArrivedInfo);
        header.setComponentAlignment(teacherArrivedInfo, Alignment.MIDDLE_CENTER);

        TeacherArrived last = teacherArrivedService.getLast();
        teacherArrivedInfo.setValue(UpdateableTeacherArrivedLabel.buildMessage(last));
    }

    private void setMenuBar() {
        menuBar = new MenuBar();
        menuBar.addItem("home - nadia", null);
        MenuItem searchItem = menuBar.addItem("search", null);
        searchItem.addItem("stuff", null);
        searchItem.addItem("more stuff", null);
        menuBar.setWidth("100%");
        this.addComponent(menuBar);
    }

    private void setCommets() {
        comments.setSizeFull();
        comments.setSpacing(true);
        comments.setMargin(true);
        crearTablaDepartamentos();
        this.addComponent(comments);
    }

    private void setHeader() {
        header.setSizeFull();
        header.setSpacing(true);
        header.setMargin(true);
        this.addComponent(header);
    }

    private void setTeacherArrivedButton() {
        teacherArrivedButton = new Button("Profesor llego", FontAwesome.CHECK);
        teacherArrivedButton.setSizeUndefined();
        teacherArrivedButton.setHeight("100%");
        header.addComponent(teacherArrivedButton);
        header.setComponentAlignment(teacherArrivedButton, Alignment.MIDDLE_CENTER);

        teacherArrivedButton
                .addClickListener(evt -> {
                    String userName = getSession().getAttribute("user").toString();
                    teacherArrivedController.run(TeacherArrivedController.Action.TEACHER_ARRIVED,
                            userName);
                    System.out.println("Agregado " + teacherArrivedService.getLast());
                });
    }

    private void setTeacherLeftButton() {
        teacherLeftButton = new Button("Profesor se fue", FontAwesome.POWER_OFF);
        teacherLeftButton.setSizeUndefined();
        teacherLeftButton.setHeight("100%");
        header.addComponent(teacherLeftButton);
        header.setComponentAlignment(teacherLeftButton, Alignment.MIDDLE_CENTER);

        teacherLeftButton.addClickListener(evt -> {
            String userName = getSession().getAttribute("user").toString();
            teacherArrivedController.run(TeacherArrivedController.Action.TEACHER_LEFT, userName);
            System.out.println("Agregado " + teacherArrivedService.getLast());
        });
    }

    private void setTitle() {
        Label title = new Label("Aforo");
        title.setSizeUndefined();
        title.setStyleName("h1");
        header.addComponent(title);
        header.setComponentAlignment(title, Alignment.TOP_RIGHT);
        header.setExpandRatio(title, 1);
    }

    // private void setApprovalButton() {
    // approvalButton = new Button("", FontAwesome.THUMBS_UP);
    // approvalButton.setSizeUndefined();
    // approvalButton.setHeight("100%");
    // comments.addComponent(approvalButton);
    // comments.setComponentAlignment(approvalButton, Alignment.MIDDLE_CENTER);
    //
    // approvalButton.addClickListener(evt -> {
    // String userName = getSession().getAttribute("user").toString();
    // System.out.println(userName+" Aprobo el cometario");
    // });
    // }
    //
    // private void setDisapprovalButton() {
    // disapprovalButton = new Button("", FontAwesome.THUMBS_DOWN);
    // disapprovalButton.setSizeUndefined();
    // disapprovalButton.setHeight("100%");
    // comments.addComponent(disapprovalButton);
    // comments.setComponentAlignment(disapprovalButton, Alignment.MIDDLE_CENTER);
    //
    // disapprovalButton.addClickListener(evt -> {
    // String userName = getSession().getAttribute("user").toString();
    // System.out.println(userName+" Desaprobo el cometario");
    // });
    // }
    //
    // private void setOldComment() {
    // Label user = new Label("@Martin");
    // Label comment = new Label("MMM....me encantan las boobies ;)");
    // user.setSizeUndefined();
    // comment.setSizeUndefined();
    // user.setStyleName("oldUserComments");
    // comment.setStyleName("oldComments");
    // comments.addComponent(user);
    // comments.addComponent(comment);
    // comments.setComponentAlignment(user, Alignment.TOP_LEFT);
    // comments.setComponentAlignment(comment, Alignment.TOP_LEFT);
    // comments.setExpandRatio(user, 1);
    // comments.setExpandRatio(comment, 1);
    // }

    private void setOldComment() {
        OldComment oldComment = new OldComment();
        comments.addComponent(oldComment.getOldComment());
    }

    private void setNewComment() {
        // TextField comment = new TextField("@" + getSession().getAttribute("user").toString());
        TextField comment = new TextField("@User:");
        comment.setValue("Martin es un niño Booby :p");
        // Handle changes in the value
        comment.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                // Assuming that the value type is a String
                String value = (String) event.getProperty().getValue();
                // Do something with the value
                Notification.show("Value is: " + value);
                System.out.println("El usuario ingreso: " + value);
            }
        });

        // Fire value changes immediately when the field loses focus
        comment.setImmediate(true);
        comment.setSizeUndefined();
        comments.addComponent(comment);
        // header.setComponentAlignment(comment, Alignment.MIDDLE_CENTER);
        comments.setExpandRatio(comment, 1);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        System.out.println("entering prueba view....");
        teacherArrivedController.addUpdateable(getSession().getAttribute("user"),
                new UpdateableTeacherArrivedLabel(teacherArrivedInfo));
    }

    // private void crearTablaDepartamentos() {
    // // Create a container for such beans with
    // // strings as item IDs.
    // BeanContainer<String, Departamento> departamentos =
    // new BeanContainer<String, Departamento>(Departamento.class);
    //
    // // Use the name property as the item ID of the bean
    // departamentos.setBeanIdProperty("name");
    //
    // // Add some beans to it
    // departamentos.addBean(new Departamento("Computacion", "Algoritmos y programacion I"));
    // departamentos.addBean(new Departamento("Matematicas", "Probabilidad y estadistica"));
    // departamentos.addBean(new Departamento("Fisica", "Fisica III"));
    //
    // // Bind a table to it
    // Table table = new Table("Materias", departamentos);
    //
    // //layout.addComponent(table);
    // comments.addComponent(table);
    // }

    private void crearTablaDepartamentos() {
        /*
         * You can read or set the currently selected item by the value property of the Tree
         * component, that is, with getValue() and setValue(). When the user clicks an item on a
         * tree, the tree will receive an ValueChangeEvent, which you can catch with a
         * ValueChangeListener. To receive the event immediately after the click, you need to set
         * the tree as setImmediate(true).
         * 
         * The Tree component uses Container data sources much like the Table component, with the
         * addition that it also utilizes hierarchy information maintained by a
         * HierarchicalContainer. The contained items can be of any item type supported by the
         * container. The default container and its addItem() assume that the items are strings and
         * the string value is used as the item ID.
         */
        final Object[][] departamentos = new Object[][] {
                new Object[] { "Matematicas", "Probabilidad y estadisticas B" },
                new Object[] { "Fisica", "Fisica I", "Fisica II" },
                new Object[] { "Computacion", "Algoritmos y programacion I",
                        "Taller de programacion I", "Aplicaciones informaticas" },
                new Object[] { "Electronica", "Laboratorio", "Organizacion de las computadoras" } };

        Tree tree = new Tree("Catedras");

        /* Add planets as root items in the tree. */
        for (int i = 0; i < departamentos.length; i++) {
            String depto = (String) (departamentos[i][0]);
            tree.addItem(depto);

            if (departamentos[i].length == 1) {
                // The planet has no moons so make it a leaf.
                tree.setChildrenAllowed(depto, false);
            } else {
                // Add children (moons) under the planets.
                for (int j = 1; j < departamentos[i].length; j++) {
                    String catedra = (String) departamentos[i][j];

                    // Add the item as a regular item.
                    tree.addItem(catedra);

                    // Set it to be a child.
                    tree.setParent(catedra, depto);

                    // Make the moons look like leaves.
                    tree.setChildrenAllowed(catedra, false);
                }

                // Expand the subtree.
                tree.expandItemsRecursively(depto);
            }
        }
        comments.addComponent(tree);
    }
}
