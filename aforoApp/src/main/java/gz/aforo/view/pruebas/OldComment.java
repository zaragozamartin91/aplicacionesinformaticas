package gz.aforo.view.pruebas;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Esta clase se encarga de setear todos los viejos comentarios y devolver el VerticalLayout a agregar.
 * 
 * @author Galli
 *
 */
public class OldComment {

    private VerticalLayout comments = new VerticalLayout();
    
    public OldComment() {
        comments.setSizeFull();
        comments.setSpacing(true);
        comments.setMargin(true);
        setComment("@Martin", "MMM....me encantan las boobies ;)"," 2 ","0");
        setComment("@Nadia", "Martin es un pervertodo sexy"," 1 ","0");
    }
    
    public VerticalLayout getOldComment() {
        return comments;
    }
    
    private void setComment(String nameUser, String text, String nApproval, String nDisapproval) {
        setTextComment(nameUser, text);
        HorizontalLayout buttons = new HorizontalLayout();
        setButton(buttons, nApproval, FontAwesome.THUMBS_UP);
        setButton(buttons, nDisapproval, FontAwesome.THUMBS_DOWN);
        comments.addComponent(buttons);
        comments.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
    }
    
    private void setTextComment(String nameUser, String text) {
        Label user = new Label(nameUser);
        Label comment = new Label(text);
        user.setSizeUndefined();
        comment.setSizeUndefined();
        user.setStyleName("oldUserComments");
        comment.setStyleName("oldComments");
        comments.addComponent(user);
        comments.addComponent(comment);
        comments.setComponentAlignment(user, Alignment.TOP_LEFT);
        comments.setComponentAlignment(comment, Alignment.TOP_LEFT);
        comments.setExpandRatio(user, 1);
        comments.setExpandRatio(comment, 1);
    }
    
//    private void setApprovalButton(HorizontalLayout buttons, String n) {
//        Button approvalButton = new Button("", FontAwesome.THUMBS_UP);
//        Label number = new Label(n);
//        number.setSizeUndefined();
//        approvalButton.setSizeUndefined();
//        approvalButton.setHeight("100%");
//        buttons.addComponent(approvalButton);
//        buttons.setComponentAlignment(approvalButton, Alignment.MIDDLE_CENTER);
//        buttons.addComponent(number);
//        buttons.setComponentAlignment(number, Alignment.TOP_LEFT);
//        buttons.setExpandRatio(number, 1);
//        approvalButton.addClickListener(evt -> {
//            //String userName = getSession().getAttribute("user").toString();
//            System.out.println(" Aprobo el cometario");
//        });
//    }
    
    private void setButton(HorizontalLayout buttons, String n, FontAwesome imagen) {
        Button approvalButton = new Button("", imagen);
        Label number = new Label(n);
        number.setSizeUndefined();
        approvalButton.setSizeUndefined();
        approvalButton.setHeight("100%");
        buttons.addComponent(approvalButton);
        buttons.setComponentAlignment(approvalButton, Alignment.MIDDLE_CENTER);
        buttons.addComponent(number);
        buttons.setComponentAlignment(number, Alignment.TOP_LEFT);
        buttons.setExpandRatio(number, 1);
        approvalButton.addClickListener(evt -> {
            //String userName = getSession().getAttribute("user").toString();
            System.out.println(" Apreto boton en el cometario");
        });
    }

//    private void setDisapprovalButton(HorizontalLayout buttons, String n) {
//        Button disapprovalButton = new Button("", FontAwesome.THUMBS_DOWN);
//        Label number = new Label(n);
//        disapprovalButton.setSizeUndefined();
//        disapprovalButton.setHeight("100%");
//        buttons.addComponent(disapprovalButton);
//        buttons.setComponentAlignment(disapprovalButton, Alignment.MIDDLE_CENTER);
//        buttons.addComponent(number);
//        buttons.setComponentAlignment(number, Alignment.TOP_LEFT);
//        buttons.setExpandRatio(number, 1);
//        disapprovalButton.addClickListener(evt -> {
//            //String userName = getSession().getAttribute("user").toString();
//            System.out.println(" Desaprobo el cometario");
//        });
//    }
    
}
