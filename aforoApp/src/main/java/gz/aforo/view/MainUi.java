package gz.aforo.view;

import gz.aforo.config.RunMode;
import gz.aforo.config.spring.SpringBootstrap;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("mytheme")
@Widgetset("gz.aforo.MyAppWidgetset")
public class MainUi extends UI {
	private Navigator navigator;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		SpringBootstrap.get();

		if (navigatorNotSet()) {
			buildNavigator();
		}

		firstNavigate();
	}

	private boolean navigatorNotSet() {
		return navigator == null;
	}

	private void firstNavigate() {
		// final RunMode runMode = (RunMode)
		// SpringBootstrap.get().bean("runMode");

		if (getSession().getAttribute("user") == null) {
			this.getNavigator().navigateTo("login");
		} else {
			this.getNavigator().navigateTo("home");
		}
	}

	private void buildNavigator() {
		navigator = new Navigator(this, this);
		navigator.addView("login", (View) SpringBootstrap.get().bean("loginView"));

		navigator.addView("main", (View) SpringBootstrap.get().bean("subjectHomeView"));
		navigator.addView("home", (View) SpringBootstrap.get().bean("homeView"));
		navigator.addView("file", (View) SpringBootstrap.get().bean("filesView"));
		navigator.addView("encuesta", (View) SpringBootstrap.get().bean("encuestaView"));
		navigator.addView("opiniones", (View) SpringBootstrap.get().bean("opinionesView"));

		this.setNavigator(navigator);
	}

	@WebServlet(urlPatterns = { "/aforo/*", "/VAADIN/*" }, name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MainUi.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}
