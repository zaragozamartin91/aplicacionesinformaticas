package gz.aforo.view.files;

import com.vaadin.data.Item;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;

import gz.aforo.config.spring.SpringBootstrap;
import gz.aforo.controller.TeacherArrivedController;
import gz.aforo.service.TeacherArrivedService;
import gz.aforo.view.SessionKeys;
import gz.aforo.view.menubar.AforoMenubar;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component("filesView")
@Scope("prototype")
public class FilesView extends VerticalLayout implements View {
    public static final String STYLE_NAME = "main";
    public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";

    private HorizontalLayout header = new HorizontalLayout();
    private MenuBar menuBar;
    private VerticalLayout commentsLayout = new VerticalLayout();
    private Label title;
    private Table table;
    private Button buttonAddFile;
    
    @Autowired
    private ApplicationContext applicationContext;

//    @Autowired
//    @Qualifier("teacherArrivedDatabaseController")
//    private TeacherArrivedController teacherArrivedController;
//
//    @Autowired
//    @Qualifier("teacherArrivedDatabaseService")
//    private TeacherArrivedService teacherArrivedService;

    public FilesView() {
        this.setStyleName(STYLE_NAME);

        this.setWidth("100%");
        this.setHeightUndefined();
    }

    @PostConstruct
    private void initialize() {
        setHeader();
        setTitle();
        setMenuBar();
        createButtonAddFile();

        // se agregan comentarios de prueba
        setCommentsLayout();
        //Creo el menu
        crearTablaArchivos();
    }

    private void setCommentsLayout() {
        this.commentsLayout.setSpacing(true);
        this.commentsLayout.setMargin(true);
        this.addComponent(commentsLayout);
    }

    private void setMenuBar() {
        menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
        this.addComponent(menuBar);
    }

    private void setHeader() {
        header.setSizeFull();
        header.setSpacing(true);
        header.setMargin(true);
        this.addComponent(header);
    }
    
    private String getCurrUser() {
        String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
        return userName;
    }

    private void setTitle() {
        title = new Label("Aforo");
        title.setSizeUndefined();
        title.setStyleName("h1");
        header.addComponent(title);
        header.setComponentAlignment(title, Alignment.TOP_RIGHT);
        header.setExpandRatio(title, 1);
    }
    
    private String getCurrentSubject(){
        return Optional.ofNullable(getSession().getAttribute("subject").toString()).orElse(DEFAULT_SUBJECT);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        System.out.println("entering files view....");

        Object upId = getCurrentSubject();
        //teacherArrivedController.addUpdateable(upId, new UpdateableTeacherArrivedLabel(teacherArrivedInfo));
        title.setValue(getCurrUser() + "::" + getCurrentSubject());
    }
    
    private void crearTablaArchivos() {
         table = new Table("Archivos");
    
         // Define two columns for the built-in container
         table.addContainerProperty("Nombre", String.class, null);
         table.addContainerProperty("Autor", String.class, null);
         table.addContainerProperty("Clasificacion",  String.class, null);
         table.addContainerProperty("Archivo",  Button.class, null);
        
         // Add a row the hard way
//         Object newItemId = table.addItem();
//         Item row1 = table.getItem(newItemId);
//         row1.getItemProperty("Nombre").setValue("Sirius");
//         row1.getItemProperty("Clasificacion").setValue(-1.46f);
//        
//         // Add a few other rows using shorthand addItem()
//         table.addItem(new Object[]{"Canopus",        -0.72f}, 2);
//         table.addItem(new Object[]{"Arcturus",       -0.04f}, 3);
//         table.addItem(new Object[]{"Alpha Centauri", -0.01f}, 4);
         
         addItemTable("Apunte de estadistica", "Maine", "Apunte", "ruta1");
         addItemTable("Probabilidad y estadistica", "Murray R.Spiegel", "Libro", "ruta23");
         addItemTable("Diapositivas de la clase 1", "Catedra", "Documento de la catedra", "ruta15");
        
         // Show exactly the currently contained rows (items)
         table.setPageLength(table.size());
         
         table.setSizeFull();
         table.setSelectable(true);
         table.setMultiSelect(true);
         table.setImmediate(true);
         
         table.addValueChangeListener(e -> {
             System.out.println("Deberiamos abrir el archivo: " + String.valueOf(e.getProperty().getValue()));
         });
         
         commentsLayout.addComponent(table);
         commentsLayout.setComponentAlignment(table, Alignment.MIDDLE_CENTER);
         commentsLayout.setExpandRatio(table, 1);
    }
    
    @SuppressWarnings("unchecked")
    private void addItemTable(String nombre, String autor, String clasificacion, String ruta) {
        Object newItemId = table.addItem();
        Item row1 = table.getItem(newItemId);
        row1.getItemProperty("Nombre").setValue(nombre);
        row1.getItemProperty("Autor").setValue(autor);
        row1.getItemProperty("Clasificacion").setValue(clasificacion);
        row1.getItemProperty("Archivo").setValue(createButtonFile(ruta));
    }
    
    private Button createButtonFile(String ruta) {
        Button button = new Button("", FontAwesome.FILE_ARCHIVE_O);
        button.setSizeUndefined();
        button.setHeight("100%");
        button.addClickListener(evt -> {
            String userName = getSession().getAttribute("user").toString();
            Notification.show(userName+" quiere abrir el archivo "+ ruta, "", Type.TRAY_NOTIFICATION);
            System.out.println(userName+" quiere abrir el archivo " + ruta);
        });
        return button;
    }
    
    private void createButtonAddFile() {
        buttonAddFile = new Button("Agregar un nuevo archivo", FontAwesome.FILE);
        buttonAddFile.setSizeUndefined();
        buttonAddFile.setHeight("100%");
        buttonAddFile.addClickListener(evt -> {
            String userName = getSession().getAttribute("user").toString();
            Notification.show(userName+" quiere agregar un nuevo archivo", "", Type.TRAY_NOTIFICATION);
            System.out.println(userName+" quiere agregar un nuevo archivo");
            getUI().getNavigator().addView("addFile", (View) SpringBootstrap.get().bean("addFilesView"));
            getUI().getNavigator().navigateTo("addFile");
        });
        commentsLayout.addComponent(buttonAddFile);
    }
}
