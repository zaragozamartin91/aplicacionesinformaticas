package gz.aforo.view.files;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

import gz.aforo.view.SessionKeys;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component("addFilesView")
@Scope("prototype")
public class AddFilesView extends VerticalLayout implements View {
    public static final String STYLE_NAME = "main";
    public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";

    private HorizontalLayout header = new HorizontalLayout();
    private MenuBar menuBar;
    private VerticalLayout commentsLayout = new VerticalLayout();
    private Label title;
    private Button buttonTerminar;
    private TextArea nameFile;
    private TextArea autorFile;
    private ComboBox clasFile;
    private TextArea dirFile;
    private String sClasFile;

    @Autowired
    private ApplicationContext applicationContext;

    public AddFilesView() {
        this.setStyleName(STYLE_NAME);

        this.setWidth("100%");
        this.setHeightUndefined();
    }

    @PostConstruct
    private void initialize() {
        setHeader();
        setTitle();
        setMenuBar();
        setCommentsLayout();
        setInfo();
        createButtonAddFile();
    }

    private void setCommentsLayout() {
        this.commentsLayout.setSpacing(true);
        this.commentsLayout.setMargin(true);
        this.addComponent(commentsLayout);
    }

    private TextArea createTextArea() {
        TextArea text = new TextArea();
        text.setRows(1);
        // text.setRows(10);
        text.setImmediate(true);
        text.setSizeFull();
        return text;
    }

    private ComboBox createComboBoxClasificacion() {
        ComboBox combo = new ComboBox("");
        combo.addItem("Apunte");
        combo.addItem("Tutorial");
        combo.addItem("Libro");
        combo.addItem("Codigo");
        combo.addItem("Diapositivas catedra");
        combo.addValueChangeListener(e -> {
            sClasFile = (String) e.getProperty().getValue();
        });
        return combo;
    }

    private void setInfo() {
        Label name = new Label("Nombre");
        nameFile = createTextArea();
        Label autor = new Label("Autor");
        autorFile = createTextArea();
        Label clas = new Label("Clasificacion");
        clasFile = createComboBoxClasificacion();
        Label dir = new Label("Direccion archivo");
        dirFile = createTextArea();
        commentsLayout.addComponent(name);
        commentsLayout.addComponent(nameFile);
        commentsLayout.addComponent(autor);
        commentsLayout.addComponent(autorFile);
        commentsLayout.addComponent(clas);
        commentsLayout.addComponent(clasFile);
        commentsLayout.addComponent(dir);
        commentsLayout.addComponent(dirFile);
    }

    private void setMenuBar() {
        menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
        this.addComponent(menuBar);
    }

    private void setHeader() {
        header.setSizeFull();
        header.setSpacing(true);
        header.setMargin(true);
        this.addComponent(header);
    }

    private String getCurrUser() {
        String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
        return userName;
    }

    private void setTitle() {
        title = new Label("Aforo");
        title.setSizeUndefined();
        title.setStyleName("h1");
        header.addComponent(title);
        header.setComponentAlignment(title, Alignment.TOP_RIGHT);
        header.setExpandRatio(title, 1);
    }

    private String getCurrentSubject() {
        return Optional.ofNullable(getSession().getAttribute("subject").toString()).orElse(
                DEFAULT_SUBJECT);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        System.out.println("entering files view....");

        Object upId = getCurrentSubject();
        // teacherArrivedController.addUpdateable(upId, new
        // UpdateableTeacherArrivedLabel(teacherArrivedInfo));
        title.setValue(getCurrUser() + "::" + getCurrentSubject());
    }

    private void createButtonAddFile() {
        buttonTerminar = new Button("Agregar un nuevo archivo", FontAwesome.CHECK);
        buttonTerminar.setSizeUndefined();
        buttonTerminar.setHeight("100%");
        buttonTerminar.addClickListener(evt -> {
            String userName = getSession().getAttribute("user").toString();
            String sNameFile = nameFile.getValue();
            String sAutorFile = autorFile.getValue();
            String sDirFile = dirFile.getValue();
            Notification.show(userName + " quiere agregar un nuevo archivo llamado: " + sNameFile
                    + "; autor: " + sAutorFile + "; clasificacion: " + sClasFile + "; ruta: "
                    + sDirFile, "", Type.TRAY_NOTIFICATION);
            System.out.println(userName + " quiere agregar un nuevo archivo");
        });
        commentsLayout.addComponent(buttonTerminar);
    }
}
