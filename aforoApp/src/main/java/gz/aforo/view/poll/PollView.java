package gz.aforo.view.poll;

import gz.aforo.model.ReviewsComment;
import gz.aforo.service.ReviewsCommentService;
import gz.aforo.view.SessionKeys;

import java.util.Date;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("encuestaView")
@Scope("prototype")
public class PollView extends VerticalLayout implements View {
    public static final String STYLE_NAME = "main";
    public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";

    private HorizontalLayout header = new HorizontalLayout();
    private MenuBar menuBar;
    private VerticalLayout mainLayout = new VerticalLayout();
    private FormLayout formMainLayout;
    private OptionGroup calificacionCursada;
    private OptionGroup calificacionProfesores;
    private TextArea observacionesCursada;
    private TextArea observacionesProfesores;
    private TextArea sugerencia;
    
    private String RcalificacionCursada;
    private String RcalificacionProfesores;
    private String RobservacionesCursada;
    private String RobservacionesProfesores;
    private String Rsugerencia;
    
    @Autowired
    @Qualifier("reviewsCommentDatabaseService")
    private ReviewsCommentService commentService;

//    @Autowired
//    @Qualifier("commentDatabaseService")
//    private CommentService commentService;
    private Label title;
    
    @Autowired
    private ApplicationContext applicationContext;

    public PollView() {
        this.setStyleName(STYLE_NAME);
        this.setWidth("100%");
        this.setHeightUndefined();
    }

    @PostConstruct
    private void initialize() {
        setHeader();
        setTitle();
        setMenuBar();
        setMainLayout();
    }

	private void setMainLayout() {
		this.addComponent(mainLayout);
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
	}

    private void setMenuBar() {
        menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
        this.addComponent(menuBar);
    }

    private void setHeader() {
        header.setSizeFull();
        header.setSpacing(true);
        header.setMargin(true);
        this.addComponent(header);
    }

    private String getCurrUser() {
        String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
        return userName;
    }

    private void setTitle() {
        title = new Label("Aforo");
        title.setSizeUndefined();
        title.setStyleName("h1");
        header.addComponent(title);
        header.setComponentAlignment(title, Alignment.TOP_RIGHT);
        header.setExpandRatio(title, 1);
    }

    private String getCurrentSubject() {
        if (getSession() == null) {
            return DEFAULT_SUBJECT;
        }
        return Optional.ofNullable(getSession().getAttribute(SessionKeys.SUBJECT).toString()).orElse(DEFAULT_SUBJECT);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        System.out.println("entering EncuestaView....");

        title.setValue(getCurrUser() + "@" + getCurrentSubject());
        
        resetMainFormLayout();
        
        selectCalificationCursada();
        setObservacionesCursada();
        selectCalificationProfesores();
        setObservacionesProfesores();
        setObservacionesSugerencia();
        setSubmitButton();
    }

	private void resetMainFormLayout() {
		try {
			mainLayout.removeComponent(formMainLayout);
		} catch (Exception e) {
		}
        formMainLayout = new FormLayout();
        this.formMainLayout.setSpacing(true);
        this.formMainLayout.setMargin(true);
        mainLayout.addComponent(formMainLayout);
	}
    
    private OptionGroup addCalification(OptionGroup calificacion, int i, String title) {
        calificacion.addItem(i);
        calificacion.setItemCaption(i, title);
        return calificacion;
    }
    
    private OptionGroup createCalification(OptionGroup calificacion) {
        addCalification(calificacion, 1, "Mala");
        addCalification(calificacion, 2, "Regular");
        addCalification(calificacion, 3, "Buena");
        addCalification(calificacion, 4, "Muy buena");
        addCalification(calificacion, 5, "Excelente");
        calificacion.select(2);
        calificacion.setNullSelectionAllowed(false);
        calificacion.setHtmlContentAllowed(true);
        calificacion.setImmediate(true);
        return calificacion;
    }
    
    private void selectCalificationCursada() {
        calificacionCursada = new OptionGroup("Califique la cursada: ");
        createCalification(calificacionCursada);
        formMainLayout.addComponent(calificacionCursada);
        calificacionCursada.addValueChangeListener(e -> {
            //System.out.println("Opcion seleccionada: "+e.getProperty().getValue());
            RcalificacionCursada = String.valueOf(calificacionCursada.getItemCaption(e.getProperty().getValue()));
        });
    }
    
    private void selectCalificationProfesores() {
        calificacionProfesores = new OptionGroup("Califique el desempeño de los profesores: ");
        createCalification(calificacionProfesores);
        formMainLayout.addComponent(calificacionProfesores);
        calificacionProfesores.addValueChangeListener(e -> {
            //System.out.println("Opcion seleccionada: "+e.getProperty().getValue());
            RcalificacionProfesores = String.valueOf(calificacionProfesores.getItemCaption(e.getProperty().getValue()));
        });
    }
    
    private void setObservacionesCursada(){
//        HorizontalLayout observaciones = new HorizontalLayout();
//        Label tituloObservaciones = new Label("Observaciones: ");
//        tituloObservaciones.setSizeUndefined();
        observacionesCursada = new TextArea("Observaciones");
        observacionesCursada.setValue("");
        observacionesCursada.setRows(5);
        observacionesCursada.setImmediate(true);
        observacionesCursada.setSizeFull();
//        observaciones.addComponent(tituloObservaciones);
//        observaciones.addComponent(observacionesCursada);
//        formMainLayout.addComponent(observaciones);
        formMainLayout.addComponent(observacionesCursada);
        observacionesCursada.addValueChangeListener(e -> {
            RobservacionesCursada = (String) e.getProperty().getValue();
            System.out.println("Observaciones de la cursada: "+e.getProperty().getValue());
        });
    }
    
    private void setObservacionesProfesores(){
//        HorizontalLayout observaciones = new HorizontalLayout();
//        Label tituloObservaciones = new Label("Observaciones: ");
//        tituloObservaciones.setSizeUndefined();
        observacionesProfesores = new TextArea("Observaciones profesores");
        observacionesProfesores.setValue("");
        observacionesProfesores.setRows(5);
        observacionesProfesores.setImmediate(true);
        observacionesProfesores.setSizeFull();
//        observaciones.addComponent(tituloObservaciones);
//        observaciones.addComponent(observacionesProfesores);
//        formMainLayout.addComponent(observaciones);
        formMainLayout.addComponent(observacionesProfesores);
        observacionesProfesores.addValueChangeListener(e -> {
            RobservacionesProfesores = (String) e.getProperty().getValue();
            System.out.println("Observaciones de los profesores: "+e.getProperty().getValue());
        });
    }
    
    private void setObservacionesSugerencia(){
//        HorizontalLayout observaciones = new HorizontalLayout();
//        Label tituloObservaciones = new Label("Sugerencia para los alumnos: ");
//        tituloObservaciones.setSizeUndefined();
        sugerencia = new TextArea("Sugerencia para los alumnos:");
        sugerencia.setValue("");
        sugerencia.setRows(5);
        sugerencia.setImmediate(true);
        sugerencia.setSizeFull();
//        observaciones.addComponent(tituloObservaciones);
//        observaciones.addComponent(sugerencia);
//        formMainLayout.addComponent(observaciones);
        formMainLayout.addComponent(sugerencia);
        sugerencia.addValueChangeListener(e -> {
            Rsugerencia = (String) e.getProperty().getValue();
            System.out.println("Sugerencia para los alumnos: "+e.getProperty().getValue());
        });
    }
    
    private void setSubmitButton(){
        Button button = new Button("Terminar", FontAwesome.HACKER_NEWS);
//        button.setSizeUndefined();
//        button.setHeight("100%");
        button.setSizeFull();
        button.addClickListener(evt -> {
            //Eliminar el registro que esta anotado en esa catedra
            addInfo();
            String userName = getSession().getAttribute("user").toString();
            System.out.println(userName+" Completo la encuesta");
            Notification.show(userName+" completo la encuesta", "", Type.TRAY_NOTIFICATION);
            getUI().getNavigator().navigateTo("home");
        });
//        formMainLayout.addComponent(button);
        mainLayout.addComponent(button);
    }
    
    private void addInfo() {
        String userName = getSession().getAttribute("user").toString();
        String comment = "Calificacion de la cursada: "+RcalificacionCursada;
        if(RobservacionesCursada == null)
            RobservacionesCursada = "-";
        comment += "<br/>Observaciones de la cursada: "+RobservacionesCursada;
        comment += "<br/>Calificacion de los profesores: "+RcalificacionProfesores;
        if(RobservacionesProfesores == null)
            RobservacionesProfesores = "-";
        comment += "<br/>Observaciones de los profesores: "+RobservacionesProfesores;
        if(Rsugerencia == null)
            Rsugerencia = "-";
        comment += "<br/>Sugerencia al ingresante: "+Rsugerencia;
        commentService.addComment(new ReviewsComment(userName, getCurrentSubject(), new Date(), comment));
    }
    
}
