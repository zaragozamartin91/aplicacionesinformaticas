package gz.aforo.view.menubar;

import gz.aforo.config.spring.SpringBootstrap;
import gz.aforo.controller.LoginController;
import gz.aforo.service.GroupService;
import gz.aforo.view.SessionKeys;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@SuppressWarnings("serial")
@Component("aforoMenubar")
@Scope("prototype")
public class AforoMenubar extends MenuBar {
	private MenuItem homeItem;
	private MenuItem newsItem;
	private MenuItem groupsItem;
	private MenuItem profileItem;
	private MenuItem selectSubjectItem;
	private MenuItem selectUserItem;

	@Autowired
    @Qualifier("databaseLoginController")
    private LoginController loginController;
	
	@Autowired
	@Qualifier("groupDatabaseService")
	private GroupService groupService;

	private final Command menuCommandHome = new Command() {
		@Override
		public void menuSelected(final MenuItem selectedItem) {
			getUI().getNavigator().navigateTo("home");
		}
	};
	
	private final Command menuCommandCreateGroup = new Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            Notification.show("Se intenta agregar un nuevo grupo. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
//            getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
//            getUI().getNavigator().navigateTo("group");
        }
    };

	private final Command menuCommandSelectGroup = new Command() {
		@Override
		public void menuSelected(final MenuItem selectedItem) {
			// Aca deberiamos obtener el nombre del grupo seleccionado y se lo
			// pasamos pero antes hay que agregarlos al menuBar
			String group = selectedItem.getText();
			getSession().setAttribute("subject", group);
			getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
			getUI().getNavigator().navigateTo("group");
		}
	};
	
	private final Command menuCommandPerfil = new Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            Notification.show("Se intenta ingresar al perfil. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
//            getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
//            getUI().getNavigator().navigateTo("group");
        }
    };
    
    private final Command menuCommandNovedades = new Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            Notification.show("Se intenta ingresar a novedades. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
//            getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
//            getUI().getNavigator().navigateTo("group");
        }
    };
    
    private final Command menuCommandSelectSubject = new Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            //Notification.show("Intenta leer opiniones de la catedra "+ selectedItem.getText() +". Futura implementacion.", "", Type.TRAY_NOTIFICATION);
            String opinionesCatedra = selectedItem.getText();
            getSession().setAttribute("subject", opinionesCatedra);
			// getUI().getNavigator().addView("opiniones", (View)
			// SpringBootstrap.get().bean("opinionesView"));
            getUI().getNavigator().navigateTo("opiniones");
        }
    };
    
    private final Command menuCommandConnectUser = new Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            Notification.show("El usuario se intenta conectar con "+selectedItem.getText()+". Futura implementacion.", "", Type.TRAY_NOTIFICATION);
//            getUI().getNavigator().addView("group", (View) SpringBootstrap.get().bean("groupView"));
//            getUI().getNavigator().navigateTo("group");
        }
    };

	private final Command menuCommandClose = new Command() {
		@Override
		public void menuSelected(final MenuItem selectedItem) {
			// Esto lo hago para que se pierda toda la informacion del login
//			getUI().getNavigator().addView("login", (View) SpringBootstrap.get().bean("loginView"));
			getSession().setAttribute(SessionKeys.USERNAME, null);
			// Abro el login
			getUI().getNavigator().navigateTo("login");
		}
	};

	@PostConstruct
	private void initialize() {
		homeItem = this.addItem("Inicio", menuCommandHome);
		newsItem = this.addItem("Novedades", menuCommandNovedades);
		selectSubjectItem = this.addItem("Opiniones de catedras", null);
		addSubject();
		groupsItem = this.addItem("Grupos", null);
		setGroupItem();
		selectUserItem = this.addItem("Miembros", null);
		addUsers();
		profileItem = this.addItem("Perfil", menuCommandPerfil);
		profileItem = this.addItem("Cerrar sesion", menuCommandClose);
		this.setWidth("100%");
	}
	
	private void addUsers() {
	    List<String> users = new ArrayList<>();//loginController.getUsers();
	    users.add("Nadia");
	    users.add("Martin");
	    users.add("Nico");
	    for (String user : users) {
	        selectUserItem.addItem(user, menuCommandConnectUser);
	    }
    }

    private void addSubject() {
	    final Object[][] departamentos = new Object[][] {
                new Object[] { "Matematicas", "Probabilidad y estadisticas B" },
                new Object[] { "Fisica", "Fisica I", "Fisica II" },
                new Object[] { "Computacion", "Algoritmos y programacion I",
                        "Taller de programacion I", "Aplicaciones informaticas" },
                new Object[] { "Electronica", "Laboratorio", "Organizacion de las computadoras" } };
	    
	    for (int i = 0; i < departamentos.length; i++) {
            String depto = (String) (departamentos[i][0]);
            MenuItem deptoItem = selectSubjectItem.addItem(depto, null);
            for (int j = 1; j < departamentos[i].length; j++) {
                String catedra = (String) departamentos[i][j];
                MenuItem catedraItem = deptoItem.addItem(catedra, menuCommandSelectSubject);
                deptoItem.addSeparatorBefore(catedraItem);
            }
        }
	}

	private void setGroupItem() {
	    groupsItem.addItem(" + CREAR UN NUEVO GRUPO", menuCommandCreateGroup);
		List<String> groups = groupService.getGroupsNames();
		groups.forEach(group -> groupsItem.addItem(group, menuCommandSelectGroup));
	}
}
