package gz.aforo.view.subject.home;

import gz.aforo.model.TeacherArrived;
import gz.aforo.model.Updateable;

import com.vaadin.ui.Label;

public class UpdateableTeacherArrivedLabel implements Updateable {
	private Label toUpdate;
	
	@Override
	public void update(Object... objects) {
		TeacherArrived teacherArrived = (TeacherArrived) objects[0];
		String msg = buildMessage(teacherArrived); 
		
		toUpdate.setValue( msg );
	}

	public static String buildMessage(TeacherArrived teacherArrived) {
		String msg = "El profesor ";
		msg += teacherArrived.getArrived() ? " ha llegado a las "+teacherArrived.getLastUpdate() : "no ha llegado...";
		return msg;
	}

	public UpdateableTeacherArrivedLabel(Label toUpdate) {
		super();
		this.toUpdate = toUpdate;
	}
}
