package gz.aforo.view.subject.home;

import gz.aforo.controller.AddSubjectCommentController;
import gz.aforo.controller.SubjectCommentController;
import gz.aforo.controller.TeacherArrivedController;
import gz.aforo.model.SubjectComment;
import gz.aforo.model.SubjectInfo;
import gz.aforo.model.TeacherArrived;
import gz.aforo.service.SubjectCommentService;
import gz.aforo.service.SubjectInfoService;
import gz.aforo.service.TeacherArrivedService;
import gz.aforo.service.UsersSubjectsService;
import gz.aforo.view.SessionKeys;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Component("subjectHomeView")
@Scope("prototype")
public class SubjectHomeView extends VerticalLayout implements View {
	public static final String STYLE_NAME = "main";
	public static final String DEFAULT_SUBJECT = "UNKNOWN_SUBJECT";
	private static final int COMMENTS_LIMIT = 5;

	private HorizontalLayout header;
	private Button teacherArrivedButton;
	private Button teacherLeftButton;
	private MenuBar menuBar;
	private Label teacherArrivedInfo;
	private VerticalLayout subjectInfoLayout;
	private VerticalLayout commentsLayout;
	private HorizontalLayout openFilesLayout;
	private Button openFilesButton;
	private Button leaveGroupButton;
	private Button joinGroupButton;

	private VerticalLayout addCommentLayout;

	@Autowired
	@Qualifier("teacherArrivedDatabaseController")
	private TeacherArrivedController teacherArrivedController;

	@Autowired
	@Qualifier("teacherArrivedDatabaseService")
	private TeacherArrivedService teacherArrivedService;

	@Autowired
	@Qualifier("subjectCommentDatabaseService")
	private SubjectCommentService commentService;
	private Label title;

	@Autowired
	@Qualifier("subjectCommentDatabaseController")
	private SubjectCommentController subjectCommentController;

	@Autowired
	@Qualifier("addSubjectCommentDatabaseController")
	private AddSubjectCommentController addSubjectCommentController;

	@Autowired
	@Qualifier("usersSubjectsDatabaseService")
	private UsersSubjectsService usersSubjectsService;

	@Autowired
	@Qualifier("subjectInfoDatabaseService")
	private SubjectInfoService subjectInfoService;

	@Autowired
	private ApplicationContext applicationContext;

	public SubjectHomeView() {
		this.setStyleName(STYLE_NAME);

		this.setWidth("100%");
		this.setHeightUndefined();
	}

	@PostConstruct
	private void initialize() {
//		setHeader();
//		setTeacherArrivedButton();
//		setTeacherLeftButton();
//		setTeacherArrivedInfo();
//		setTitle();
//		setMenuBar();
	}

	private void setSubjectInfo() {
		try {
			this.removeComponent(subjectInfoLayout);
		} catch (Exception e) {
		}

		String currentSubject = getCurrentSubject();
		SubjectInfo subjectInfo = subjectInfoService.getSubjectInfo(currentSubject);

		String subjectInfoContent = subjectInfo.getInfo();
		Label subjectInfoLabel = new Label(subjectInfoContent, ContentMode.HTML);
		subjectInfoLayout = new VerticalLayout(subjectInfoLabel);
		subjectInfoLayout.setMargin(true);

		this.addComponent(subjectInfoLayout);
	}

	private void setRecentComments() {
		try {
			this.removeComponent(commentsLayout);
		} catch (Exception e) {
		}

		/* Se resetea la seccion de comentarios */
		commentsLayout = new VerticalLayout();

		this.commentsLayout.setSpacing(true);
		this.commentsLayout.setMargin(true);
		this.addComponent(commentsLayout);

		String subjectName = getCurrentSubject();

		List<SubjectComment> urgentComments = commentService.getUrgentComments(subjectName, COMMENTS_LIMIT);
		List<SubjectComment> bestComments = commentService.getBestComments(subjectName, COMMENTS_LIMIT);
		List<SubjectComment> worstComments = commentService.getWorstComments(subjectName, COMMENTS_LIMIT);

		VerticalLayout urgentCommentsLayout = new VerticalLayout();
		urgentCommentsLayout.setSpacing(true);
		urgentComments.forEach(comm -> {
			SubjectCommentLayout commentLayout = new SubjectCommentLayout(comm);
			commentLayout.setController(subjectCommentController, this::setRecentComments);
			urgentCommentsLayout.addComponent(commentLayout);
		});
		commentsLayout.addComponent(urgentCommentsLayout);

		VerticalLayout bestCommentsLayout = new VerticalLayout();
		bestCommentsLayout.setSpacing(true);
		bestComments.forEach(comm -> {
			SubjectCommentLayout commentLayout = new SubjectCommentLayout(comm);
			commentLayout.setController(subjectCommentController, this::setRecentComments);
			bestCommentsLayout.addComponent(commentLayout);
		});
		commentsLayout.addComponent(bestCommentsLayout);

		VerticalLayout worstCommentsLayout = new VerticalLayout();
		worstCommentsLayout.setSpacing(true);
		worstComments.forEach(comm -> {
			SubjectCommentLayout commentLayout = new SubjectCommentLayout(comm);
			commentLayout.setController(subjectCommentController, this::setRecentComments);
			worstCommentsLayout.addComponent(commentLayout);
		});
		commentsLayout.addComponent(worstCommentsLayout);

	}

	private void setTeacherArrivedInfo() {
		teacherArrivedInfo = new Label();
		teacherArrivedInfo.setSizeUndefined();
		header.addComponent(teacherArrivedInfo);
		header.setComponentAlignment(teacherArrivedInfo, Alignment.MIDDLE_CENTER);

		TeacherArrived last = teacherArrivedService.getLast();
		teacherArrivedInfo.setValue(UpdateableTeacherArrivedLabel.buildMessage(last));
	}

	private void setMenuBar() {
		menuBar = (MenuBar) applicationContext.getBean("aforoMenubar");
		this.addComponent(menuBar);
	}

	private void setHeader() {
		header = new HorizontalLayout();
		
		header.setSizeFull();
		header.setSpacing(true);
		header.setMargin(true);
		
		this.addComponent(header);
	}

	private void setTeacherArrivedButton() {
		teacherArrivedButton = new Button("Profesor llego", FontAwesome.CHECK);
		teacherArrivedButton.setSizeUndefined();
		teacherArrivedButton.setHeight("100%");
		header.addComponent(teacherArrivedButton);
		header.setComponentAlignment(teacherArrivedButton, Alignment.MIDDLE_CENTER);

		String currentSubject = getCurrentSubject();
		String userName = getCurrUser();

		teacherArrivedButton.addClickListener(evt -> {
			teacherArrivedController.run(TeacherArrivedController.Action.TEACHER_ARRIVED, currentSubject, userName);
			System.out.println("Agregado " + teacherArrivedService.getLast());
		});
	}

	private void setTeacherLeftButton() {
		teacherLeftButton = new Button("Profesor se fue", FontAwesome.POWER_OFF);
		teacherLeftButton.setSizeUndefined();
		teacherLeftButton.setHeight("100%");
		header.addComponent(teacherLeftButton);
		header.setComponentAlignment(teacherLeftButton, Alignment.MIDDLE_CENTER);

		String userName = getCurrUser();
		String currentSubject = getCurrentSubject();

		teacherLeftButton.addClickListener(evt -> {
			teacherArrivedController.run(TeacherArrivedController.Action.TEACHER_LEFT, currentSubject, userName);
			System.out.println("Agregado " + teacherArrivedService.getLast());
		});
	}

	private String getCurrUser() {
		String userName = getSession().getAttribute(SessionKeys.USERNAME).toString();
		return userName;
	}

	private void setTitle() {
		title = new Label("Aforo");
		title.setSizeUndefined();
		title.setStyleName("h2");
		header.addComponent(title);
		header.setComponentAlignment(title, Alignment.TOP_RIGHT);
		header.setExpandRatio(title, 1);
	}

	private String getCurrentSubject() {
		if (getSession() == null) {
			return DEFAULT_SUBJECT;
		}
		return Optional.ofNullable(getSession().getAttribute(SessionKeys.SUBJECT).toString()).orElse(DEFAULT_SUBJECT);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		System.out.println("entering MainView....");
		
		try {
			this.removeAllComponents();
		} catch (Exception e) {
		}
		
		setHeader();
		setTeacherArrivedButton();
		setTeacherLeftButton();
		setTeacherArrivedInfo();
		setTitle();
		setMenuBar();

		if (!isMember()) {
			if (joinGroupButton == null) {
				notMember();
			}
			return;
		}
		Object upId = getCurrentSubject();
		teacherArrivedController.addUpdateable(upId, new UpdateableTeacherArrivedLabel(teacherArrivedInfo));

		setSubjectInfo();
		setOpenFiles();
		setLeaveGroupButton();

		setAddComment();
		setRecentComments();

		title.setValue(getCurrUser() + "@" + getCurrentSubject());
	}

	private void setAddComment() {
		try {
			this.removeComponent( addCommentLayout );
		} catch (Exception e1) {
		}
		
		addCommentLayout = new VerticalLayout();
		addCommentLayout.setSizeUndefined();
		addCommentLayout.setWidth("100%");
		addCommentLayout.setSpacing(true);
		addCommentLayout.setMargin(true);

		final TextArea newCommentTextArea = new TextArea("Nuevo comentario");
		newCommentTextArea.setSizeFull();
		addCommentLayout.addComponent(newCommentTextArea);

		CheckBox isUrgentCheckBox = new CheckBox("Es urgente");
		isUrgentCheckBox.setSizeUndefined();
		Button submitButton = new Button("Agregar nuevo comentario");
		submitButton.setSizeFull();
		HorizontalLayout urgentAndSubmitLayout = new HorizontalLayout(isUrgentCheckBox, submitButton);
		urgentAndSubmitLayout.setSizeFull();
		urgentAndSubmitLayout.setExpandRatio(submitButton, 1);
		urgentAndSubmitLayout.setSpacing(true);
		urgentAndSubmitLayout.setComponentAlignment(isUrgentCheckBox, Alignment.MIDDLE_CENTER);

		submitButton.addClickListener(evt -> {
			String content = newCommentTextArea.getValue();
			String userName = getCurrUser();
			String subject = getCurrentSubject();
			Boolean isUrgent = Optional.ofNullable(isUrgentCheckBox.getValue()).orElse(false);
			SubjectComment newComment = new SubjectComment(userName, subject, content).setUrgent(isUrgent);

			boolean error = false;
			try {
				this.addSubjectCommentController.addComment(newComment);
			} catch (Exception e) {
				error = true;
			}

			this.setRecentComments();

			newCommentTextArea.setValue("");
			isUrgentCheckBox.setValue(false);

			if (error) {
				Notification.show("Error al agregar comentario...", Type.ERROR_MESSAGE);
			} else {
				Notification.show("Comentario agregado exitosamente");
			}
		});

		addCommentLayout.addComponent(urgentAndSubmitLayout);
		this.addComponent(addCommentLayout);
	}

	private void setOpenFiles() {
		try {
			this.removeComponent(openFilesLayout);
		} catch (Exception e) {
		}

		openFilesLayout = new HorizontalLayout();
		openFilesLayout.setMargin(new MarginInfo(false, true, false, true));
		openFilesLayout.setSpacing(true);
		openFilesLayout.setSizeUndefined();
		openFilesLayout.setWidth("100%");

		openFilesButton = new Button("Buscar archivos", FontAwesome.FILE);
		openFilesButton.setSizeUndefined();
		// openFiles.setHeight("100%");
		openFilesButton.setSizeFull();
		openFilesLayout.addComponent(openFilesButton);
		// openFilesLayout.setComponentAlignment(openFiles,
		// Alignment.TOP_LEFT);

		openFilesButton.addClickListener(evt -> {
			System.out.println("Abrir archivos...");
			getUI().getNavigator().navigateTo("file");
		});

		this.addComponent(openFilesLayout);
	}

	private void notMember() {
		HorizontalLayout joinGroupButtonLayout = new HorizontalLayout();
		joinGroupButtonLayout.setSpacing(true);
		joinGroupButtonLayout.setMargin(true);
		joinGroupButton = new Button("Enviar solicitud de aceptación al grupo", FontAwesome.ASTERISK);
		joinGroupButton.setSizeUndefined();
		joinGroupButton.setHeight("100%");
		joinGroupButton.addClickListener(evt -> {
			String userName = getSession().getAttribute("user").toString();
			Notification.show("El usuario " + userName + " intenta unirse al grupo. Futura implementacion.", "", Type.TRAY_NOTIFICATION);
		});
		joinGroupButtonLayout.addComponent(joinGroupButton);

		this.addComponent(joinGroupButtonLayout);
		this.setComponentAlignment(joinGroupButtonLayout, Alignment.MIDDLE_CENTER);
	}

	private void setLeaveGroupButton() {
		leaveGroupButton = new Button("Salir del grupo", FontAwesome.TRASH_O);
		leaveGroupButton.setSizeFull();
		leaveGroupButton.addClickListener(evt -> {
			// getUI().getNavigator().addView("encuesta", (View)
			// SpringBootstrap.get().bean("encuestaView"));
				getUI().getNavigator().navigateTo("encuesta");
			});
		// TODO Hay que ver donde colocarlo!
		openFilesLayout.addComponent(leaveGroupButton);
		// this.setComponentAlignment(openFilesLayout,
		// Alignment.MIDDLE_RIGHT);
	}

	private boolean isMember() {
		String currUser = getCurrUser();
		String currentSubject = getCurrentSubject();
		return usersSubjectsService.isMember(currentSubject, currUser);
	}

}
