package gz.aforo.service;

import gz.aforo.model.SubjectInfo;

public interface SubjectInfoService {

	public abstract SubjectInfo getSubjectInfo(String subjectName);

}