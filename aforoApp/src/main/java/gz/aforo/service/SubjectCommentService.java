package gz.aforo.service;

import gz.aforo.model.SubjectComment;

import java.util.List;

public interface SubjectCommentService {
	public static final int ALL_COMMENTS = -1;

	/**
	 * Obtiene los comentarios de un usuario.
	 * 
	 * @param userName
	 *            - Nombre de usuario.
	 * @param limit
	 *            - Limite de registros a buscar.
	 * @return Lista de comentarios de un usuario.
	 */
	public abstract List<SubjectComment> getCommentsFrom(String userName, int limit);

	/**
	 * Obtiene los comentarios de un usuario.
	 * 
	 * @param userName
	 *            - Nombre de usuario.
	 * @param subjectName
	 *            - Nombre de catedra de comentarios buscados.
	 * @param limit
	 *            - Limite de registros a buscar.
	 * @return Lista de comentarios de un usuario.
	 */
	public List<SubjectComment> getCommentsFrom(String userName, String subjectName, int limit);

	/**
	 * Obtiene los comentarios de una materia.
	 * 
	 * @param subjectName
	 *            - Nombre de catedra de comentarios buscados.
	 * @param limit
	 *            - Limite de registros a buscar.
	 * @return Lista de comentarios de una materia.
	 */
	public List<SubjectComment> getCommentsOf(String subjectName, int limit);

	/**
	 * Aumenta en uno la cantidad de likes de un comentario.
	 * 
	 * @param comment - Comentario a aumentar likes.
	 */
	void augmentLikes(SubjectComment comment);

	/**
     * Aumenta en uno la cantidad de dislikes de un comentario.
     * 
     * @param comment - Comentario a aumentar dislikes.
     */
	void augmentDislikes(SubjectComment comment);

    /**
     * Obtiene los 'peores' comentarios.
     * 
     * @param subjectName - Nombre de materia o catedra a buscar comentarios.
     * @param limit - Cantidad de comentarios maxmios a obtener.
     * @return Peores comentarios segun materia o catedra.
     */
    List<SubjectComment> getWorstComments(String subjectName, int limit);

    /**
     * Obtiene los 'mejores' comentarios.
     * 
     * @param subjectName - Nombre de materia o catedra a buscar comentarios.
     * @param limit - Cantidad de comentarios maxmios a obtener.
     * @return mejores comentarios segun materia o catedra.
     */
    List<SubjectComment> getBestComments(String subjectName, int limit);

    void addComment(SubjectComment newComment);

	List<SubjectComment> getUrgentComments(String subjectName, int limit);

}