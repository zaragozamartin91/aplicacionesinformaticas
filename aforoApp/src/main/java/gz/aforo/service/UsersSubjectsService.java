package gz.aforo.service;

public interface UsersSubjectsService {

	/**
	 * Obtiene la cantidad de usuarios inscriptos en una materia o catedra.
	 * 
	 * @param subjectName
	 *            - Nombre de materia o catedra.
	 * @return cantidad de usuarios inscriptos en una materia o catedra.
	 */
	Integer getUsersCount(String subjectName);

	/**
	 * Verifica si un usuario es miembro de una catedra o materia.
	 * 
	 * @param subjectName
	 *            - Nombre de catedra o materia.
	 * @param userName
	 *            - Nombre de usuario.
	 * @return True en caso afirmativo, false en caso contrario.
	 */
	Boolean isMember(String subjectName, String userName);

}