package gz.aforo.service;

import gz.aforo.model.GroupComment;
import gz.aforo.model.SubjectComment;

import java.util.List;

public interface GroupCommentService {
    
    public List<GroupComment> getCommentsFrom(String userName, String groupName, int limit);
    
    public List<GroupComment> getCommentsOf(String groupName, int limit);
    
    public void addComment(GroupComment newComment);
    
}
