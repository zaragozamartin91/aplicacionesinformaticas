package gz.aforo.service;

import gz.aforo.model.TeacherArrived;

public interface TeacherArrivedService {

	/**
	 * Obtiene el ultimo registro del arribo del profesor.
	 * 
	 * @return ultimo registro del arribo del profesor, null si no encuentra
	 *         alguno.
	 */
	public abstract TeacherArrived getLast();

	/**
	 * Agrega un registro del arribo del profesor.
	 * 
	 * @param teacherArrived
	 *            - Registro a agregar.
	 */
	public abstract void setArrived(TeacherArrived teacherArrived);

	/**
	 * Obtiene el ultimo registro relacionado con una catedra o materia
	 * determinada.
	 * 
	 * @param subject
	 *            - Codigo/nombre de catedra o materia.
	 * @return Ultimo registro de llegada del profesor para dicha catedra, null
	 *         si no encuentra alguno.
	 */
	public abstract TeacherArrived getLast(String subject);

}