package gz.aforo.service.impl;

import gz.aforo.model.SubjectComment;
import gz.aforo.service.SubjectCommentService;
import gz.aforo.service.UsersSubjectsService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("subjectCommentDatabaseService")
@Scope("singleton")
public class SubjectCommentDatabaseService implements SubjectCommentService {
	@Value("${commentsTable}")
	private String tableName;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	@Qualifier("usersSubjectsDatabaseService")
	private UsersSubjectsService usersSubjectsService;

	private Connection getConnection() {
		return (Connection) applicationContext.getBean("dbConnection");
	}

	private List<SubjectComment> runQuery(String sql) {
		Connection connection = getConnection();
		List<SubjectComment> result = new ArrayList<>();
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			System.out.println("A punto de correr: " + sql);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				SubjectComment comment = mapResultSetToComment(rs);
				result.add(comment);
			}

			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}

		return result;
	}

	@Override
	public List<SubjectComment> getCommentsFrom(String userName, int limit) {
		String sql = limit == ALL_COMMENTS ? "select * from " + tableName + " where user='" + userName + "' order by postDate desc"
				: "select * from comments where user='" + userName + "' order by postDate desc limit " + limit;

		return runQuery(sql);
	}

	@Override
	public List<SubjectComment> getCommentsFrom(String userName, String subjectName, int limit) {
		String sql = "SELECT * FROM " + tableName + " WHERE user='" + userName + "' AND subject='" + subjectName
				+ "' ORDER BY postDate DESC";
		sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

		return runQuery(sql);
	}

	@Override
	public List<SubjectComment> getCommentsOf(String subjectName, int limit) {
		String sql = "SELECT * FROM " + tableName + " WHERE subject='" + subjectName + "' ORDER BY postDate DESC";
		sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

		return runQuery(sql);
	}

	@Override
	public void augmentLikes(SubjectComment comment) {
		Integer commentId = comment.getId();
		String sql = "UPDATE " + tableName + " SET LIKES=LIKES+1 WHERE id=" + commentId;
		runUpdate(sql);
	}

	@Override
	public void augmentDislikes(SubjectComment comment) {
		Integer commentId = comment.getId();
		String sql = "UPDATE " + tableName + " SET DISLIKES=DISLIKES+1 WHERE id=" + commentId;
		runUpdate(sql);
	}

	@Override
	public List<SubjectComment> getWorstComments(String subjectName, int limit) {
		Integer criteria = getCriteria(subjectName);

		List<SubjectComment> comments = this.getCommentsOf(subjectName, limit);
		List<SubjectComment> collect = comments.stream().filter(comm -> comm.getLikes() < criteria && comm.isNotUrgent())
				.collect(Collectors.toList());

		return collect;
	}

	@Override
	public List<SubjectComment> getBestComments(String subjectName, int limit) {
		Integer criteria = getCriteria(subjectName);

		List<SubjectComment> comments = this.getCommentsOf(subjectName, limit);
		List<SubjectComment> collect = comments.stream().filter(comm -> comm.getLikes() >= criteria && comm.isNotUrgent())
				.collect(Collectors.toList());
		collect.forEach(comm -> comm.setBest(true));

		return collect;
	}

	@Override
	public List<SubjectComment> getUrgentComments(String subjectName, int limit) {
		List<SubjectComment> comments = this.getCommentsOf(subjectName, limit);
		List<SubjectComment> collect = comments.stream().filter(comm -> comm.isUrgent()).collect(Collectors.toList());
		collect.forEach(comm -> comm.setBest(true));

		return collect;
	}

	/*
	 * id user subject postDate content likes dislikes
	 */
	@Override
	public void addComment(SubjectComment newComment) {
		Connection connection = (Connection) applicationContext.getBean("dbConnection");
		try {
			String sql = "INSERT INTO " + tableName + "(user,subject,postDate,content,likes,dislikes,urgent) VALUES(?,?,NOW(),?,?,?,?)";
			PreparedStatement st = connection.prepareStatement(sql);

			st.setString(1, newComment.getUserName());
			st.setString(2, newComment.getSubject());
			st.setString(3, newComment.getContent());
			st.setInt(4, newComment.getLikes());
			st.setInt(5, newComment.getDislikes());
			st.setBoolean(6, newComment.isUrgent());

			System.out.println("A punto de correr " + sql);
			st.executeUpdate();

			st.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
	}

	protected Integer getCriteria(String subjectName) {
		Integer usersCount = usersSubjectsService.getUsersCount(subjectName);

		Double d = (double) ((double) usersCount / 2);
		Integer criteria = ((Double) Math.ceil(d)).intValue();
		return criteria;
	}

	public static void main(String[] args) {
		Integer arr[] = { 1, 2, 3, 4, 5 };
		List<Integer> comments = Arrays.asList(arr);
		Integer criteria = 3;
		List<Integer> collect = comments.stream().filter(comm -> comm >= criteria).collect(Collectors.toList());
		System.out.println(collect);
	}

	private void runUpdate(String sql) {
		try {
			Connection connection = getConnection();
			PreparedStatement st = connection.prepareStatement(sql);
			System.out.println("A punto de correr: " + sql);
			st.executeUpdate();

			st.close();
			connection.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private SubjectComment mapResultSetToComment(ResultSet rs) throws SQLException {
		Integer id = rs.getInt("id");
		String user = rs.getString("user");
		String subject = rs.getString("subject");
		Date postDate = rs.getTimestamp("postDate");
		String content = rs.getString("content");
		Integer likes = rs.getInt("likes");
		Integer dislikes = rs.getInt("dislikes");
		Boolean urgent = rs.getBoolean("urgent");
		return new SubjectComment(id, user, subject, postDate, content, likes, dislikes).setUrgent(urgent);
	}
}
