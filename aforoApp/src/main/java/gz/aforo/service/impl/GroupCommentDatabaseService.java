package gz.aforo.service.impl;

import gz.aforo.model.GroupComment;
import gz.aforo.model.SubjectComment;
import gz.aforo.service.GroupCommentService;
import gz.aforo.service.SubjectCommentService;
import gz.aforo.service.UsersSubjectsService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("groupCommentDatabaseService")
@Scope("singleton")
public class GroupCommentDatabaseService implements GroupCommentService {
    private static final int ALL_COMMENTS = -1;

    @Value("${groupCommentsTable}")
    private String tableName;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("usersSubjectsDatabaseService")
    private UsersSubjectsService usersSubjectsService;

    private Connection getConnection() {
        return (Connection) applicationContext.getBean("dbConnection");
    }

    private List<GroupComment> runQuery(String sql) {
        Connection connection = getConnection();
        List<GroupComment> result = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            System.out.println("A punto de correr: " + sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                GroupComment comment = mapResultSetToComment(rs);
                result.add(comment);
            }

            rs.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }

        return result;
    }

    @Override
    public List<GroupComment> getCommentsFrom(String userName, String groupName, int limit) {
        String sql = "SELECT * FROM " + tableName + " WHERE user='" + userName + "' AND groupName='" + groupName
                + "' ORDER BY postDate DESC";
        sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

        return runQuery(sql);
    }

    @Override
    public List<GroupComment> getCommentsOf(String groupName, int limit) {
        String sql = "SELECT * FROM " + tableName + " WHERE groupName='" + groupName + "' ORDER BY postDate DESC";
        sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

        return runQuery(sql);
    }

    /*
     * id user subject postDate content likes dislikes
     */
    @Override
    public void addComment(GroupComment newComment) {
        Connection connection = (Connection) applicationContext.getBean("dbConnection");
        try {
            String sql = "INSERT INTO " + tableName
                    + "(user,groupName,postDate,content) VALUES(?,?,NOW(),?)";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, newComment.getUserName());
            st.setString(2, newComment.getGroupName());
            st.setString(3, newComment.getContent());

            System.out.println("A punto de correr " + sql);
            st.executeUpdate();

            st.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    private void runUpdate(String sql) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(sql);
            System.out.println("A punto de correr: " + sql);
            st.executeUpdate();

            st.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private GroupComment mapResultSetToComment(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String user = rs.getString("user");
        String group = rs.getString("groupName");
        Date postDate = rs.getTimestamp("postDate");
        String content = rs.getString("content");
        return new GroupComment(user, group, postDate, content);
    }
}
