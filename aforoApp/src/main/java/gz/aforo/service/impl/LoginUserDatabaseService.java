package gz.aforo.service.impl;

import gz.aforo.model.LoginUser;
import gz.aforo.service.LoginUserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Busca usuarios en la base de datos.
 * 
 * @author martin
 *
 */
@Service
@Scope("singleton")
public class LoginUserDatabaseService implements LoginUserService {
	@Autowired
	private ApplicationContext applicationContext;

	@Value("${loginTable}")
	private String loginTableName;

	@Override
	public LoginUser getUser(String name) {
		Connection connection = (Connection) applicationContext.getBean("dbConnection");
		try {
			PreparedStatement st = connection.prepareStatement("SELECT * FROM " + loginTableName + " WHERE name=?");
			st.setString(1, name);
			ResultSet rs = st.executeQuery();

			LoginUser foundUser = null;

			if (rs.next()) {
				foundUser = new LoginUser(rs.getString("name"), rs.getString("pass"));
			}

			rs.close();
			st.close();

			if (foundUser == null) {
				throw new Exception("User " + name + " not found!");
			} else {
				return foundUser;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeConnection(connection);
		}
	}

	private void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}
}
