package gz.aforo.service.impl;

import gz.aforo.model.ReviewsComment;
import gz.aforo.service.ReviewsCommentService;
import gz.aforo.service.UsersSubjectsService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("reviewsCommentDatabaseService")
@Scope("singleton")
public class ReviewsCommentDatabaseService implements ReviewsCommentService {
    private static final int ALL_COMMENTS = -1;

    @Value("${reviewsCommentsTable}")
    private String tableName;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("usersSubjectsDatabaseService")
    private UsersSubjectsService usersSubjectsService;

    private Connection getConnection() {
        return (Connection) applicationContext.getBean("dbConnection");
    }

    private List<ReviewsComment> runQuery(String sql) {
        Connection connection = getConnection();
        List<ReviewsComment> result = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            System.out.println("A punto de correr: " + sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                ReviewsComment comment = mapResultSetToComment(rs);
                result.add(comment);
            }

            rs.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }

        return result;
    }

    @Override
    public List<ReviewsComment> getCommentsFrom(String userName, String subject, int limit) {
        String sql = "SELECT * FROM " + tableName + " WHERE user='" + userName + "' AND subject='" + subject
                + "' ORDER BY postDate DESC";
        sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

        return runQuery(sql);
    }

    @Override
    public List<ReviewsComment> getCommentsOf(String subject, int limit) {
        String sql = "SELECT * FROM " + tableName + " WHERE subject='" + subject + "' ORDER BY postDate DESC";
        sql += limit == ALL_COMMENTS ? " LIMIT " + limit : "";

        return runQuery(sql);
    }

    @Override
    public void addComment(ReviewsComment newComment) {
        Connection connection = (Connection) applicationContext.getBean("dbConnection");
        try {
            String sql = "INSERT INTO " + tableName
                    + "(user,subject,postDate,content) VALUES(?,?,NOW(),?)";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, newComment.getUser());
            st.setString(2, newComment.getSubject());
            st.setString(3, newComment.getContent());

            System.out.println("A punto de correr " + sql);
            st.executeUpdate();

            st.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    private void runUpdate(String sql) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(sql);
            System.out.println("A punto de correr: " + sql);
            st.executeUpdate();

            st.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private ReviewsComment mapResultSetToComment(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("id");
        String user = rs.getString("user");
        String subject = rs.getString("subject");
        Date postDate = rs.getTimestamp("postDate");
        String content = rs.getString("content");
        return new ReviewsComment(user, subject, postDate, content);
    }
}
