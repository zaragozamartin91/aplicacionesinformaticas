package gz.aforo.service.impl;

import gz.aforo.model.SubjectInfo;
import gz.aforo.service.SubjectInfoService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("subjectInfoDatabaseService")
@Scope("singleton")
public class SubjectInfoDatabaseService implements SubjectInfoService {
	@Value("${subjectInfoTable}")
	private String tableName;

	@Autowired
	private ApplicationContext applicationContext;

	private Connection newConnection() {
		return (Connection) applicationContext.getBean("dbConnection");
	}

	@Override
	public SubjectInfo getSubjectInfo(String subjectName) {
		SubjectInfo subjectInfo = new SubjectInfo();
		subjectInfo.setName(subjectName);
		
		try {
			Connection connection = newConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE subject=?");
			ps.setString(1, subjectName);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				subjectInfo.setInfo(rs.getString(2));
			} else {
				throw new RuntimeException("No se encontro informacion para la materia/catedra " + subjectName);
			}

			rs.close();
			ps.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return subjectInfo;
	}
}
