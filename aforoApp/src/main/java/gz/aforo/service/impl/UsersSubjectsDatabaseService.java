package gz.aforo.service.impl;

import gz.aforo.service.UsersSubjectsService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("usersSubjectsDatabaseService")
@Scope("singleton")
public class UsersSubjectsDatabaseService implements UsersSubjectsService {
	@Autowired
	private ApplicationContext applicationContext;

	@Value("users_subjects")
	private String tableName;

	@Override
	public Integer getUsersCount(String subjectName) {
		try {
			Connection connection = (Connection) applicationContext.getBean("dbConnection");
			PreparedStatement st = connection
					.prepareStatement("SELECT COUNT(*) FROM " + tableName + " WHERE subject='" + subjectName + "'");

			ResultSet rs = st.executeQuery();
			Integer result = 0;
			if (rs.next()) {
				result = rs.getInt(1);
			}

			rs.close();
			st.close();
			connection.close();
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean isMember(String subjectName, String userName) {
		try {
			Connection connection = (Connection) applicationContext.getBean("dbConnection");
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE subject='" + subjectName
					+ "' AND user='" + userName + "'");
			ResultSet rs = ps.executeQuery();

			boolean result = false;
			if (rs.next()) {
				result = true;
			}

			rs.close();
			ps.close();
			connection.close();

			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
