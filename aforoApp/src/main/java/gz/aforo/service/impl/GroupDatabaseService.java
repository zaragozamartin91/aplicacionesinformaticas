package gz.aforo.service.impl;

import gz.aforo.model.InfoGroup;
import gz.aforo.service.GroupService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("groupDatabaseService")
@Scope("singleton")
public class GroupDatabaseService implements GroupService {
	@Value("${groupTable}")
	private String tableName;

	@Autowired
	private ApplicationContext applicationContext;

	private Connection getConnection() {
		return (Connection) applicationContext.getBean("dbConnection");
	}

	private List<InfoGroup> runQuery(String sql) {
		Connection connection = getConnection();
		List<InfoGroup> result = new ArrayList<>();
		try {
			PreparedStatement st = connection.prepareStatement(sql);
			System.out.println("A punto de correr: " + sql);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
			    InfoGroup info = mapResultSetToComment(rs);
				result.add(info);
			}

			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}

		return result;
	}
	
	@Override
	public List<String> getUserOf(String groupName) {
		String sql = "SELECT * FROM " + tableName + " WHERE name='" + groupName
				+ "' ORDER BY user";
		List<InfoGroup> infos = runQuery(sql);
		List<String> result = new ArrayList<>();
		for (InfoGroup info : infos) {
		    result.add(info.getUser());
		}
		return result;
	}

	private InfoGroup mapResultSetToComment(ResultSet rs) throws SQLException {
		String user = rs.getString("user");
		String name = rs.getString("name");
		return new InfoGroup(name, user);
	}

    @Override
    public List<String> getGroupsNames() {
        String sql = "SELECT * FROM " + tableName + " ORDER BY name";
        List<InfoGroup> infos = runQuery(sql);
        List<String> result = new ArrayList<String>();
        //Hago esto solo para evitar duplicados, despues busco una mejor solucion.
        Map<String, String> map = new HashMap<String, String>();
        for (InfoGroup info : infos) {
            map.put(info.getName(), info.getName());
        }
        map.keySet().forEach(name -> {result.add(name);});
        return result;
    }
}
