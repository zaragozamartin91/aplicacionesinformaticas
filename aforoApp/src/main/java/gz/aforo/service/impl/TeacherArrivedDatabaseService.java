package gz.aforo.service.impl;

import gz.aforo.model.TeacherArrived;
import gz.aforo.service.TeacherArrivedService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class TeacherArrivedDatabaseService implements TeacherArrivedService {
	@Value("${teacherArrivedTable}")
	private String tableName;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public TeacherArrived getLast() {
		return getLast(null);
	}

	@Override
	public TeacherArrived getLast(String searchSubject) {
		Connection connection = (Connection) applicationContext.getBean("dbConnection");
		try {
			PreparedStatement st = null;

			if (searchSubject == null || searchSubject.isEmpty()) {
				String sql = "SELECT * FROM " + tableName + " ORDER BY lastUpdate DESC LIMIT 1";
				System.out.println("Corriendo " + sql);
				st = connection.prepareStatement(sql);
			} else {
				String sql = "SELECT * FROM " + tableName + " WHERE subject='" + searchSubject + "' ORDER BY lastUpdate DESC LIMIT 1";
				System.out.println("Corriendo " + sql);
				st = connection.prepareStatement(sql);
			}
			ResultSet rs = st.executeQuery();

			TeacherArrived last = null;

			if (rs.next()) {
				String subject = rs.getString("subject");
				String updateUser = rs.getString("updateUser");
				Timestamp lastUpdate = rs.getTimestamp("lastUpdate");
				boolean arrived = rs.getBoolean("arrived");
				last = new TeacherArrived(subject, updateUser, lastUpdate, arrived);
			}

			rs.close();
			st.close();

			if (last == null) {
				System.out.println("No se encontraron registros de llegada del profesor para la catedra " + searchSubject);
			}

			return last;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeConnection(connection);
		}
	}


	@Override
	public void setArrived(TeacherArrived teacherArrived) {
		Connection connection = (Connection) applicationContext.getBean("dbConnection");
		try {
			PreparedStatement st = connection.prepareStatement("INSERT INTO " + tableName + " VALUES(?,?,NOW(),?)");
			st.setString(1, teacherArrived.getSubject());
			st.setString(2, teacherArrived.getUpdateUser());
			st.setBoolean(3, teacherArrived.getArrived());
			st.executeUpdate();

			st.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeConnection(connection);
		}
	}

	private void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
