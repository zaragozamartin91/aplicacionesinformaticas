package gz.aforo.service;

import gz.aforo.model.LoginUser;

public interface LoginUserService {

	/**
	 * Retorna un usuario de login a partir de su nombre o id.
	 * 
	 * @param name - Nombre o id de usuario.
	 * @return Usuario encontrado.
	 */
	public abstract LoginUser getUser(String name);

}