package gz.aforo.service;

import gz.aforo.model.ReviewsComment;

import java.util.List;

public interface ReviewsCommentService {
    
    public List<ReviewsComment> getCommentsFrom(String userName, String subject, int limit);
    
    public List<ReviewsComment> getCommentsOf(String subject, int limit);
    
    public void addComment(ReviewsComment newComment);

}
