package gz.aforo.service;

import java.util.List;

public interface GroupService {

    /**
     * Obtiene los usuarios de un grupo.
     * 
     * @param groupName
     *            : Nombre del grupo.
     * 
     * @return Lista de usuarios de un grupo.
     */
    public List<String> getUserOf(String groupName);

    /**
     * Obtiene los nombres de los grupos existentes.
     * 
     * @return Lista de usuarios de un grupo.
     */
    public List<String> getGroupsNames();

}
