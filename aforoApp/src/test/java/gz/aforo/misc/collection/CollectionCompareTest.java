package gz.aforo.misc.collection;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class CollectionCompareTest {

	@Test
	public void testCompareLists() {
		{
			String[] arr1 = { "hola", " como  ", " estas  todo bien?" };
			String[] arr2 = { new String("hola"), " como  ", new String(" estas  todo bien?") };
			assertEquals(Arrays.asList(arr1), Arrays.asList(arr2));
		}

		{
			String[] arr1 = { "hola", " estas  todo bien?", " como  " };
			String[] arr2 = { new String("hola"), " como  ", new String(" estas  todo bien?") };
			assertNotSame(Arrays.asList(arr1), Arrays.asList(arr2));
		}
	}

}
