package gz.aforo.misc.jdbc;

import gz.aforo.config.spring.SpringMainConfig;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class FirstExample {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext appCtx = new AnnotationConfigApplicationContext(SpringMainConfig.class);
		
		Connection connection = null;
		Statement statement = null;
		try {
			// Verificamos que la clase driver este en el classpath
			Class.forName("com.mysql.jdbc.Driver");

			// abrimos la conexion
			System.out.println("Connecting to database...");
			connection = (Connection) appCtx.getBean("dbConnection");

			// ejecutamos una query
			System.out.println("Creating statement...");
			statement = (Statement) connection.createStatement();
			String sqlQuery = "SELECT id, first, last, age FROM Employees";
			ResultSet resultSet = statement.executeQuery(sqlQuery);

			// iteramos por el resultset (resultset seria como un ITERATOR en el
			// que cuando haces "next" obtenes la siguiente fila)
			while (resultSet.next()) {
				// buscamos cada valor por nombre de columna de la tabla
				int id = resultSet.getInt("id");
				int age = resultSet.getInt("age");
				String first = resultSet.getString("first");
				String last = resultSet.getString("last");

				// imprimimos los valores
				System.out.print("ID: " + id);
				System.out.print(", Age: " + age);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);
			}
			// limpiamos todos los recursos necesarios
			resultSet.close();
			statement.close();
			connection.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException se2) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");
	}
}
