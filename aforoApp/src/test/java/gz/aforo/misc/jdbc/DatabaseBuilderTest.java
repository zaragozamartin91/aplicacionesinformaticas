package gz.aforo.misc.jdbc;

import gz.aforo.config.spring.SpringBootstrap;
import gz.aforo.model.GroupComment;
import gz.aforo.model.ReviewsComment;
import gz.aforo.model.SubjectComment;
import gz.aforo.model.DataPair;
import gz.aforo.model.InfoGroup;
import gz.aforo.model.SubjectInfo;
import gz.aforo.view.subject.home.SubjectHomeView;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

public class DatabaseBuilderTest {
	private Connection connection;
	private DatabaseMetaData metaData;
	private List<DataPair> defaultUsers = new ArrayList<>();
	private List<SubjectComment> defaultComments = new ArrayList<>();
	private List<GroupComment> defaultGroupComments = new ArrayList<>();
	private List<ReviewsComment> defaultReviewsComments = new ArrayList<>();
	private List<InfoGroup> defaultGroup = new ArrayList<>();
	private List<DataPair> defaultUserSubjects = new ArrayList<>();
	private List<SubjectInfo> defaultSubjectInfo = new ArrayList<>();

	private Properties configurationProperties;
	private String loginTable;
	private String teacherArrivedTable;
	private String commentsTable;
	private String groupCommentsTable;
	private String reviewsCommentsTable;
	private String groupTable;
	private String usersSubjectsTable;
	private String subjectInfoTable;

	@Before
	public void initDefUsers() {
		defaultUsers.add(new DataPair("martin", "martin"));
		defaultUsers.add(new DataPair("nadia", "nadia"));
		defaultUsers.add(new DataPair("nico", "nico"));
		defaultUsers.add(new DataPair("n", "n"));

		defaultComments
				.add(new SubjectComment(
						"martin",
						"aninfo",
						new GregorianCalendar(2015, 3, 21, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ",
						0, 0));
		defaultComments.add(new SubjectComment("nadia", "inforga", new GregorianCalendar(2015, 10, 3, 10, 55).getTime(),
				"Este comentario es relativamente corto...", 0, 0));
		defaultComments
				.add(new SubjectComment(
						"martin",
						SubjectHomeView.DEFAULT_SUBJECT,
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ",
						0, 0));
		defaultComments.add(new SubjectComment("nadia", SubjectHomeView.DEFAULT_SUBJECT, new GregorianCalendar(2015, 6, 7, 10, 55)
				.getTime(), "Este comentario es relativamente corto...", 0, 0));
		defaultComments.add(new SubjectComment("nadia", SubjectHomeView.DEFAULT_SUBJECT,
				new GregorianCalendar(2015, 6, 8, 9, 13).getTime(),
				"Este es un comentario un toque mas largo ciertamente mas largo que algunos de los anteriores...", 0, 0));

		defaultComments
				.add(new SubjectComment(
						"martin",
						"Fisica I",
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ",
						0, 0));
		defaultComments.add(new SubjectComment("nadia", "Fisica I", new GregorianCalendar(2015, 6, 7, 10, 55).getTime(),
				"Este comentario es relativamente corto...", 0, 0));
		defaultComments.add(new SubjectComment("nadia", "Fisica I", new GregorianCalendar(2015, 6, 8, 9, 13).getTime(),
				"Este es un comentario un toque mas largo ciertamente mas largo que algunos de los anteriores...", 0, 0));

		defaultComments.add(new SubjectComment("nadia", "Probabilidad y estadisticas B", new GregorianCalendar(2015, 10, 3, 10, 55)
				.getTime(), "Este comentario es relativamente corto...", 0, 0));
		defaultComments
				.add(new SubjectComment(
						"martin",
						"Probabilidad y estadisticas B",
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ",
						0, 0));
		defaultComments.add(new SubjectComment("nadia", "Probabilidad y estadisticas B", new GregorianCalendar(2015, 6, 7, 10, 55)
				.getTime(), "Este comentario es relativamente corto...", 0, 0));
		defaultComments.add(new SubjectComment("nadia", "Probabilidad y estadisticas B",
				new GregorianCalendar(2015, 6, 8, 9, 13).getTime(),
				"Este es un comentario un toque mas largo ciertamente mas largo que algunos de los anteriores...", 5, 0));

		// new Object[] { "Matematicas", "Probabilidad y estadisticas B" },
		// new Object[] { "Fisica", "Fisica I", "Fisica II" },
		// new Object[] { "Computacion", "Algoritmos y programacion I",
		// "Taller de programacion I", "Aplicaciones informaticas" },
		// new Object[] { "Electronica", "Laboratorio",
		// "Organizacion de las computadoras" } };

		defaultUserSubjects.add(new DataPair("Probabilidad y estadisticas B", "martin"));
		defaultUserSubjects.add(new DataPair("Probabilidad y estadisticas B", "nadia"));
		// defaultUserSubjects.add( new
		// DataPair("Probabilidad y estadisticas B", "nico") );
		defaultUserSubjects.add(new DataPair("Fisica II", "martin"));
		defaultUserSubjects.add(new DataPair("Fisica II", "nadia"));
		defaultUserSubjects.add(new DataPair("Fisica II", "nico"));

		buildDefaultInfoGroupList();
		buildDefaultGroupCommentsList();
		addReviewsComments();
		buildDefaultSubjectInfo();

		configurationProperties = (Properties) SpringBootstrap.get().bean("configurationProperties");
		loginTable = configurationProperties.getProperty("loginTable");
		teacherArrivedTable = configurationProperties.getProperty("teacherArrivedTable");
		commentsTable = configurationProperties.getProperty("commentsTable");
		groupCommentsTable = configurationProperties.getProperty("groupCommentsTable");
		groupTable = configurationProperties.getProperty("groupTable");
		reviewsCommentsTable = configurationProperties.getProperty("reviewsCommentsTable");
		usersSubjectsTable = configurationProperties.getProperty("usersSubjectsTable");
		subjectInfoTable = configurationProperties.getProperty("subjectInfoTable");
		System.out.println(groupTable);
	}

	private void buildDefaultSubjectInfo() {
		defaultSubjectInfo.add(new SubjectInfo("Probabilidad y estadisticas B",
				"<p>Jefe de catedra: Teresa Gil<br/>Curso: 23<br/>Horario oficial: Lu 14-19 y Ju 14-19</p>"));
		defaultSubjectInfo.add(new SubjectInfo("Fisica II",
				"<p>Jefe de catedra: Jorge Riveros<br/>Curso: 22<br/>Horario oficial: Ma 14-19 y Ju 14-19</p>"));
	}

	private void addReviewsComments() {
		defaultReviewsComments
				.add(new ReviewsComment(
						"nico",
						"Probabilidad y estadisticas B",
						new GregorianCalendar(2015, 3, 21, 22, 23).getTime(),
						"Calificacion de la cursada: Excelente <br/>Observaciones de la cursada: - <br/>Calificacion de los profesores: Excelente <br/>Observaciones de los profesores: - <br/>Sugerencia al ingresante: Si le dan los horarios elija esta catedra."));
		defaultReviewsComments
				.add(new ReviewsComment(
						"n",
						"Probabilidad y estadisticas B",
						new GregorianCalendar(2015, 10, 3, 10, 55).getTime(),
						"Calificacion de la cursada: Muy buena <br/>Observaciones de la cursada: - <br/>Calificacion de los profesores: Excelente <br/>Observaciones de los profesores: - <br/>Sugerencia al ingresante: -"));
		defaultReviewsComments
				.add(new ReviewsComment(
						"n",
						"Fisica II",
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Calificacion de la cursada: Buena <br/>Observaciones de la cursada: - <br/>Calificacion de los profesores: Excelente <br/>Observaciones de los profesores: - <br/>Sugerencia al ingresante: -"));
	}

	private void buildDefaultGroupCommentsList() {
		defaultGroupComments
				.add(new GroupComment(
						"nico",
						"Grupo alfa-omega-lobo",
						new GregorianCalendar(2015, 3, 21, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla "));
		defaultGroupComments.add(new GroupComment("nadia", "Mordecai & los Rigbies", new GregorianCalendar(2015, 10, 3, 10, 55).getTime(),
				"Este comentario es relativamente corto..."));
		defaultGroupComments
				.add(new GroupComment(
						"martin",
						"Mordecai & los Rigbies",
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla "));
		defaultGroupComments.add(new GroupComment("nadia", "Grupo dinamita", new GregorianCalendar(2015, 6, 7, 10, 55).getTime(),
				"Este comentario es relativamente corto..."));
		defaultGroupComments.add(new GroupComment("nadia", "Grupo alfa-omega-lobo", new GregorianCalendar(2015, 6, 8, 9, 13).getTime(),
				"Este es un comentario un toque mas largo ciertamente mas largo que algunos de los anteriores..."));

		defaultGroupComments
				.add(new GroupComment(
						"martin",
						"Mordecai & los Rigbies",
						new GregorianCalendar(2015, 5, 3, 22, 23).getTime(),
						"Este es un comentario de prueba bastante largo para demostrar escencialmente como se acomodaria el texto del comentario ante distintas circunstancias locas. Mas texto, bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla "));
		defaultGroupComments.add(new GroupComment("nadia", "Mordecai & los Rigbies", new GregorianCalendar(2015, 6, 7, 10, 55).getTime(),
				"Este comentario es relativamente corto..."));
		defaultGroupComments.add(new GroupComment("nadia", "Grupo alfa-omega-lobo", new GregorianCalendar(2015, 6, 8, 9, 13).getTime(),
				"Este es un comentario un toque mas largo ciertamente mas largo que algunos de los anteriores..."));

		defaultGroupComments.add(new GroupComment("nadia", "Mordecai & los Rigbies", new GregorianCalendar(2015, 10, 3, 10, 55).getTime(),
				"Este comentario es relativamente corto..."));
	}

	private void buildDefaultInfoGroupList() {
		defaultGroup.add(new InfoGroup("Mordecai & los Rigbies", "martin"));
		defaultGroup.add(new InfoGroup("Grupo dinamita", "nadia"));
		defaultGroup.add(new InfoGroup("Grupo alfa-omega-lobo", "nadia"));
		defaultGroup.add(new InfoGroup("Mordecai & los Rigbies", "nadia"));
		defaultGroup.add(new InfoGroup("Grupo alfa-omega-lobo", "nico"));
	}

	@Test
	public void buildDatabase() throws Exception {
		try {
			connection = (Connection) SpringBootstrap.get().bean("dbConnection");
			metaData = connection.getMetaData();
		} catch (SQLException e) {
			throw new RuntimeException("Imposible conectarse con base de datos. Verificar conexion y existencia de la base...");
		}

		createSubjectInfoTable();
		buildSubjectInfoTable();

		createUsersTable();
		buildUsersTable();

		createTeacherArrivedTable();
		buildTeacherArrivedTable();

		createCommentsTable();
		buildCommentsTable();

		createGroupTable();
		buildGroupTable();

		createGroupCommentsTable();
		buildGroupCommentsTable();

		createReviewsCommentsTable();
		buildReviewsCommentsTable();

		createUsersSubjectsTable();
		buildUsersSubjectsTable();

		connection.close();
	}

	private void buildSubjectInfoTable() throws SQLException {
		System.out.println("Construyendo tabla " + subjectInfoTable + "...");

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + subjectInfoTable + " VALUES(?,?)");

		defaultSubjectInfo.forEach(subjectInfo -> {
			try {
				insertStatement.setString(1, subjectInfo.getName());
				insertStatement.setString(2, subjectInfo.getInfo());
				insertStatement.executeUpdate();
			} catch (Exception e) {
			}
		});

		insertStatement.close();
	}

	private void createSubjectInfoTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + subjectInfoTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + subjectInfoTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		System.out.println("Creando tabla " + subjectInfoTable + "...");
		PreparedStatement statement = connection.prepareStatement("CREATE TABLE " + subjectInfoTable
				+ "(subject varchar(32) not null PRIMARY KEY, info varchar(128) not null)");
		statement.executeUpdate();
		statement.close();
	}

	private void buildUsersSubjectsTable() throws SQLException {
		System.out.println("Construyendo tabla " + usersSubjectsTable + "...");

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + usersSubjectsTable
				+ "(subject,user) VALUES(?,?)");

		defaultUserSubjects.forEach(dg -> {
			try {
				System.out.println("Insertando " + dg + " en tabla " + usersSubjectsTable + "...");
				insertStatement.setString(1, dg.first.toString());
				insertStatement.setString(2, dg.second.toString());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();

	}

	private void createUsersSubjectsTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + usersSubjectsTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + usersSubjectsTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		System.out.println("Creando tabla " + usersSubjectsTable + "...");
		PreparedStatement statement = connection.prepareStatement("CREATE TABLE " + usersSubjectsTable
				+ "(subject varchar(32) not null, user varchar(32) not null, primary key(subject,user) , foreign key(user) references "
				+ loginTable + "(name) on delete NO ACTION)");
		statement.executeUpdate();
		statement.close();
	}

	private void createCommentsTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + commentsTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + commentsTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		ResultSet tables = metaData.getTables(null, null, commentsTable, null);
		if (!tables.next()) {
			System.out.println("Tabla " + commentsTable + " no encontrada. Creando...");
			PreparedStatement statement = connection
					.prepareStatement("CREATE TABLE "
							+ commentsTable
							+ "(id integer not null auto_increment primary key, user varchar(32) not null , subject varchar(32) not null ,postDate datetime ,content varchar(512) ,likes integer, dislikes integer,urgent boolean)");
			statement.executeUpdate();
			statement.close();
			return;
		}

		System.out.println("Table " + commentsTable + " encontrada!");

	}

	private void buildCommentsTable() throws SQLException {
		System.out.println("Limpiando tabla " + commentsTable);
		PreparedStatement delSt = connection.prepareStatement("DELETE FROM " + commentsTable);
		delSt.execute();
		delSt.close();

		System.out.println("Construyendo tabla " + commentsTable + "...");

		PreparedStatement st = connection.prepareStatement("SELECT * FROM " + commentsTable);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			System.out.println("Tabla " + commentsTable + " ya tiene informacion!");
			rs.close();
			st.close();
			return;
		}

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + commentsTable
				+ "(user,subject,postDate,content,likes,dislikes) VALUES(?,?,?,?,?,?)");

		defaultComments.forEach(comment -> {
			try {
				System.out.println("Insertando " + comment + " en tabla " + commentsTable + "...");
				insertStatement.setString(1, comment.getUserName());
				insertStatement.setString(2, comment.getSubject());
				insertStatement.setTimestamp(3, new Timestamp(comment.getPostDate().getTime()));
				insertStatement.setString(4, comment.getContent());
				insertStatement.setInt(5, comment.getLikes());
				insertStatement.setInt(6, comment.getDislikes());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();
	}

	private void createReviewsCommentsTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + reviewsCommentsTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + reviewsCommentsTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		ResultSet tables = metaData.getTables(null, null, reviewsCommentsTable, null);
		if (!tables.next()) {
			System.out.println("Tabla " + reviewsCommentsTable + " no encontrada. Creando...");
			PreparedStatement statement = connection
					.prepareStatement("CREATE TABLE "
							+ reviewsCommentsTable
							+ "(id integer not null auto_increment primary key, user varchar(32) not null , subject varchar(32) not null ,postDate datetime ,content varchar(512))");
			statement.executeUpdate();
			statement.close();
			return;
		}

		System.out.println("Table " + reviewsCommentsTable + " encontrada!");

	}

	private void buildReviewsCommentsTable() throws SQLException {
		System.out.println("Limpiando tabla " + reviewsCommentsTable);
		PreparedStatement delSt = connection.prepareStatement("DELETE FROM " + reviewsCommentsTable);
		delSt.execute();
		delSt.close();

		System.out.println("Construyendo tabla " + reviewsCommentsTable + "...");

		PreparedStatement st = connection.prepareStatement("SELECT * FROM " + reviewsCommentsTable);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			System.out.println("Tabla " + reviewsCommentsTable + " ya tiene informacion!");
			rs.close();
			st.close();
			return;
		}

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + reviewsCommentsTable
				+ "(user,subject,postDate,content) VALUES(?,?,?,?)");

		defaultReviewsComments.forEach(comment -> {
			try {
				System.out.println("Insertando " + comment + " en tabla " + reviewsCommentsTable + "...");
				insertStatement.setString(1, comment.getUser());
				insertStatement.setString(2, comment.getSubject());
				insertStatement.setTimestamp(3, new Timestamp(comment.getPostDate().getTime()));
				insertStatement.setString(4, comment.getContent());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();
	}

	private void createGroupTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + groupTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + groupTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		System.out.println("Creando tabla " + groupTable + "...");
		PreparedStatement statement = connection.prepareStatement("CREATE TABLE " + groupTable
				+ "(name varchar(100) not null, user varchar(32) not null, primary key(name,user) , foreign key(user) references "
				+ loginTable + "(name) on delete NO ACTION)");
		statement.executeUpdate();
		statement.close();

	}

	private void buildGroupTable() throws SQLException {
		System.out.println("Construyendo tabla " + groupTable + "...");

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + groupTable + "(name,user) VALUES(?,?)");

		defaultGroup.forEach(info -> {
			try {
				System.out.println("Insertando " + info + " en tabla " + groupTable + "...");
				insertStatement.setString(1, info.getName());
				insertStatement.setString(2, info.getUser());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();
	}

	private void createGroupCommentsTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + groupCommentsTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + groupCommentsTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		ResultSet tables = metaData.getTables(null, null, groupCommentsTable, null);
		if (!tables.next()) {
			System.out.println("Tabla " + groupCommentsTable + " no encontrada. Creando...");
			PreparedStatement statement = connection
					.prepareStatement("CREATE TABLE "
							+ groupCommentsTable
							+ "(id integer not null auto_increment primary key, user varchar(32) not null ,groupName varchar(32) not null ,postDate datetime ,content varchar(512))");
			statement.executeUpdate();
			statement.close();
			return;
		}

		System.out.println("Table " + groupCommentsTable + " encontrada!");

	}

	private void buildGroupCommentsTable() throws SQLException {
		System.out.println("Limpiando tabla " + groupCommentsTable);
		PreparedStatement delSt = connection.prepareStatement("DELETE FROM " + groupCommentsTable);
		delSt.execute();
		delSt.close();

		System.out.println("Construyendo tabla " + groupCommentsTable + "...");

		PreparedStatement st = connection.prepareStatement("SELECT * FROM " + groupCommentsTable);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			System.out.println("Tabla " + groupCommentsTable + " ya tiene informacion!");
			rs.close();
			st.close();
			return;
		}

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + groupCommentsTable
				+ "(user,groupName,postDate,content) VALUES(?,?,?,?)");

		defaultGroupComments.forEach(comment -> {
			try {
				System.out.println("Insertando " + comment + " en tabla " + groupCommentsTable + "...");
				insertStatement.setString(1, comment.getUserName());
				insertStatement.setString(2, comment.getGroupName());
				insertStatement.setTimestamp(3, new Timestamp(comment.getPostDate().getTime()));
				insertStatement.setString(4, comment.getContent());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();
	}

	private void createTeacherArrivedTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + teacherArrivedTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + teacherArrivedTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
		}

		ResultSet tables = metaData.getTables(null, null, teacherArrivedTable, null);
		if (!tables.next()) {
			System.out.println("Tabla " + teacherArrivedTable + " no encontrada. Creando...");
			PreparedStatement statement = connection.prepareStatement("create table " + teacherArrivedTable
					+ "(subject varchar(32) , updateUser varchar(32) , lastUpdate datetime,arrived bool)");
			statement.executeUpdate();
			statement.close();
			return;
		}

		System.out.println("Table " + teacherArrivedTable + " encontrada!");
	}

	private void buildTeacherArrivedTable() throws Exception {
		System.out.println("Limpiando tabla " + teacherArrivedTable);
		PreparedStatement st = connection.prepareStatement("DELETE FROM " + teacherArrivedTable);
		st.executeUpdate();

		st.close();

		System.out.println("Insertando datos en " + teacherArrivedTable);
		st = connection.prepareStatement("INSERT INTO " + teacherArrivedTable + " VALUES(?,?,NOW(),?)");
		st.setString(1, "aninfo::rivadula");
		st.setString(2, "martin");
		st.setBoolean(3, true);
		st.executeUpdate();

		st.setString(1, "inforga::ducrey");
		st.setString(2, "nadia");
		st.setBoolean(3, false);
		st.executeUpdate();

		st.setString(1, SubjectHomeView.DEFAULT_SUBJECT);
		st.setString(2, "martin");
		st.setBoolean(3, false);
		st.executeUpdate();

		st.close();
	}

	private void createUsersTable() throws SQLException {
		try {
			System.out.println("Destruyendo tabla " + loginTable);
			PreparedStatement dropSt = connection.prepareStatement("DROP TABLE " + loginTable);
			dropSt.executeUpdate();
			dropSt.close();
		} catch (Exception e) {
			System.err.println("Error al eliminar tabla " + loginTable);
		}

		ResultSet tables = metaData.getTables(null, null, loginTable, null);
		if (!tables.next()) {
			System.out.println("Tabla " + loginTable + " no encontrada. Creando...");
			PreparedStatement statement = connection.prepareStatement("create table " + loginTable
					+ "(name varchar(32) not null primary key, pass varchar(32) not null)");
			statement.executeUpdate();
			statement.close();
			return;
		}

		System.out.println("Table " + loginTable + " encontrada!");
	}

	private void buildUsersTable() throws SQLException {
		System.out.println("Construyendo tabla " + loginTable + "...");

		PreparedStatement st = connection.prepareStatement("SELECT * FROM " + loginTable);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			System.out.println("Tabla " + loginTable + " ya tiene informacion!");
			rs.close();
			st.close();
			return;
		}

		final PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO " + loginTable + " VALUES(?,?)");
		defaultUsers.forEach(usr -> {
			try {
				System.out.println("Insertando " + usr + " en tabla " + loginTable + "...");
				insertStatement.setString(1, usr.first.toString());
				insertStatement.setString(2, usr.second.toString());
				insertStatement.executeUpdate();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		insertStatement.close();
	}

}
