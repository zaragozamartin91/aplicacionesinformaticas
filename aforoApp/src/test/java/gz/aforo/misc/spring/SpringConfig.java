package gz.aforo.misc.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SpringConfig {
	@Bean(name="martin")
	@Scope("prototype")
	public Person martin(){
		return new Person("martin",1234);
	}
}
