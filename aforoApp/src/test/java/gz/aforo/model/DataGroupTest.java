package gz.aforo.model;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class DataGroupTest {

	@Test
	public void testEqualsObject() {
		assertEquals(new DataGroup("hola", "como", "estas"), new DataGroup("hola", "como", new String("estas")));
	}

	@Test
	public void testSearchWithinMap() {
		Map<DataGroup, String> map = new HashMap<>();
		map.put(new DataGroup("UNKNOWN_SUBJECT", "martin"), "uno");
		map.put(new DataGroup("UNKNOWN_SUBJECT", "nadia"), "dos");
		map.put(new DataGroup("inforga", "martin"), "tres");
		map.put(new DataGroup("aninfo", "nadia"), "cuatro");

		assertNotSame(new DataGroup("UNKNOWN_SUBJECT", "martin"), new DataGroup("inforga", "martin"));
		assertNotSame(new DataGroup("UNKNOWN_SUBJECT", "martin"), new DataGroup("UNKNOWN_SUBJECT", "nadia"));
		assertEquals(new DataGroup("aninfo", "nadia"), new DataGroup("aninfo", "nadia"));
	}
}
